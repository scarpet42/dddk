# Décortiquons Docker dans Kubernetes

Ce projet est destiné à produire un document LaTeX.

## Le but du projet est de produire un document expliquant Docker et Kubernetes

C'est un document technique destiné à des personnes ayant une bonne compréhension des systèmes d'exploitation.
Docker et Kubernetes ne sont pas particulièrement récents, mais ils commencent à être omniprésents en entreprise.
J'ai trouvé plein de présentations rapides et plein de documentations approfondies.
Je n'ai rien trouvé entre les deux.
Les présentations rapides ne sont pas utiles et il faut du temps pour lire tous les documents techniques.
J'ai donc écrit ce que j'aurais aimé trouver en faisant des recherches.

## Lecture du document compilé
Le document compilé est disponible ici : [dddk.pdf](https://scarpet42.gitlab.io/dddk/dddk.pdf)

## License

La licence est la WTFPL : [WTFPL](http://www.wtfpl.net/)
C'est la plus permissive des licences.
Si je ne le fais pas, vous n'avez rien le droit de faire du texte sans mon accord.
