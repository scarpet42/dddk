%begin-include
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Auteur : Stéphane CARPENTIER
% Fichier : multistage.tex
% 	Modif : dim. 07 janv. 2024 09:47
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\Section{La création d'images multi-stage}

Oublions un peu nix pour revenir à la construction d'images.
Si vous avez bien compris ce que j'ai expliqué avant, chaque action de la construction d'une image entraine l'ajout d'une couche.
Et une fois la couche ajoutée, elle ne peut plus être modifiée.
Pour ce que j'ai montré jusqu'ici, la limite était faible puisque je n'ai créé que des images à partir de packages existants.
Mais imaginez quelqu'un qui veut créer une image pour y mettre une application compilée créée par ses soins.
Cette personne va devoir~:

\begin{tuxliste}
\item récupérer une image existante,
\item installer le compilateur,
\item importer les sources de son application,
\item compiler son application.
\end{tuxliste}

Et ça marche.
L'inconvénient est que le compilateur, le code source ainsi que les fichiers temporaires de compilation sont devenus inutiles.
Et il n'y a plus de moyen de les enlever pour libérer de la place.
Vous allez me dire qu'il suffit de tout faire en une ligne et ça marche~: après tout, c'est ce que je fais pour \LaTeX{}.
Sauf que si l'application est un peu complexe, tout faire en une ligne va être particulièrement lourd.
Il y a donc une méthode prévue par Docker que je vais aborder maintenant.
C'est le titre de la section, c'est la création d'images multi-stages.
Le principe est de construire plusieurs images et de récupérer les fichiers d'une image pour les mettre dans une autre image.

Pour illustrer ça, je vais vous montrer une image imaginaire simplifiée qui ne marche pas puisque je n'ai rien à compiler.


\begin{monfichier}{dockerfile}{Dockerfile}
# Container initial avec un nom pour l'appeler ensuite
FROM archlinux AS source
# Installation du compilateur
RUN pacman -Syu --noconfirm gcc
# Récupération des sources de l'ordinateur vers le container
COPY /sources /tmp/workdir/sources
# Compilation
RUN make

# Container final, inutile de le nommer
FROM archlinux
# Récupération des binaires compilés du container précédant
COPY --from=source /tmp/workdir/result /bin
\end{monfichier}

Et voilà.
La première image peut être supprimée, la seconde seule est nécessaire.
Il y a une question que le lecteur attentif peut se poser~:«~Pourquoi ne pas compiler en local pour déposer le binaire sur l'image~?~».
C'est parce que rien ne me garanti que ça va fonctionner.
Mon ordinateur personnel n'a pas les mêmes fichiers installés qu'une image Docker.
Donc, lorsque je recopie le résultat de la compilation de mon ordinateur sur l'image, il peut manquer une librairie qui existe sur mon ordinateur mais n'est pas sur l'image.
Ou alors, les librairies n'ont pas forcément les mêmes versions.
Ou alors, le problème n'est pas la librairie mais une application dépendante~: vous vous souvenez de \liglat{which} qui est nécessaire pour \LaTeX{} mais n'était pas installé sur le container archlinux~?
J'avais fait des essais d'installation de \liglat{ping} d'une image ubuntu vers une image alpine mais c'était trop lourd.
Même l'exécution de la commande  \liglat{ldd} m'a montré que j'allais y passer des heures pour un résultat non garanti.
Pour quelque chose d'aussi complexe que \LaTeX{}, ça n'était pas envisageable.

Par contre, avec nix qui me garanti que je peux retrouver toutes les dépendances de mes fichiers, ça redevient envisageable.
Et même plus.
Il y a une image dont je n'ai pas encore parlé, c'est l'image scratch.
Comme son nom l'indique, elle est vide, elle n'offre que le noyau puisque c'est le principe de Docker.
Puisque la dérivation de nix garanti de récupérer la totalité des dépendances, il n'y a plus aucun intérêt à utiliser l'image alpine qui, bien que minimale, est trop grosse.
Regardons ça.

\begin{monfichier}{dockerfile}{Dockerfile}
# Nous partons de nixos
FROM nixos/nix AS source
# Création d'un répertoire pour isoler l'installation
RUN mkdir -p /extract/store
# Petit bidouillage qui ne me plaît pas
RUN nix-env --switch-profile /extract/profile
RUN rm ~/.nix-profile
RUN ln -s /extract/profile ~/.nix-profile
RUN hash -r
# Installation de curl dans l'environnement séparé
RUN nix-env --profile /extract/profile -i curl
# Isolation de curl et de ses dépendances
RUN cp -a $(nix-store -qR /extract/profile) /extract/store/

# Nous partons de rien
FROM scratch
# Récupération des fichiers
COPY --from=source /extract/store /nix/store
# Récupération des liens qui remplacent les variables d'environnement
COPY --from=source /extract/profile/ /usr/local/
# Seul curl m'intéresse, je le rends presque obligatoire
ENTRYPOINT ["curl"]
# Site optionnel à passer à la commande curl
CMD ["example.com"]
$
\end{monfichier}

Comme toujours, quelques explications s'imposent, c'est la raison d'avoir ajouté ce chapitre.
Regardons dans l'ordre.
Le \liglat{FROM}, c'est simple, c'est le nom de la distribution nixos avec tout ce qu'il faut pour installer le paquet.
Le \liglat{AS}, c'est le nom sur lequel je pourrais le référencer plus loin.
Ce n'est pas obligatoire, il est possible de référencer leurs numéros, mais je trouve ça plus propre.
De plus, ça permet d'avoir un fichier plus simplement évolutif~: si je veux rajouter une image intermédiaire, je n'ai pas à revoir la numérotation des images.

Pour le côté du bidouillage qui ne me plaît pas, ça illustre à la fois le côté pas mature de la solution, ma mauvaise maîtrise du système et la limite de la documentation.
Mon but est d'installer \liglat{curl} et de l'isoler avec toutes ses dépendances.
Sauf que les commandes Docker sont exécutées en tant qu'administrateur et que les commandes nix marchent moins bien.
Je suis donc obligé d'utiliser les profiles.

Le principe, c'est qu'il est possible d'avoir plusieurs profiles différents en fonction de ce que vous voulez faire et de basculer d'un profile à l'autre en fonction des besoins.
Par exemple, si vous voulez tester la toute dernière version d'une application.
Alors vous l'installez sur un autre profile et vous la testez sur ce profile.
Et vous pouvez rebasculer sur l'ancienne version juste en changeant de profile, sans que rien ne soit perturbé.
Sauf que la gestion des profiles est bugguée et ne marche pas comme dans la doc.
La remontée du bug a été fermée il y a longtemps par quelqu'un qui a écrit~: «~Je crois que ça marche.~»
Donc soit ça a été corrigé et la doc n'est pas mise à jour avec la bonne méthode, soit ça ne marche toujours pas.
J'en suis donc réduit à exécuter des commandes de bas niveau pour obtenir ce que les commandes de nix auraient dû faire toutes seules comme des grandes.

Ensuite, j'installe \liglat{curl}, ce qui est normal, mais je suis obligé de l'installer dans un autre profile, ce qui n'est pas normal.
Puis, je recopie tout ce qui a été installé sur le profile dans un répertoire dédié.
Normalement, il aurait dû suffire d'installer tout ce qui est relatif à \liglat{curl} mais en tant qu'administrateur ça ne marche pas.
Je dois donc installer tout ce qui a été installé sur le profile, ce qui revient au même.
Une fois mon image temporaire prête, je peux passer à la création de l'image finale.

La création de l'image est très simple.
D'abord, je pars d'une image vide puisque je n'ai besoin de rien d'autre~: nix m'apporte tout ce qui est nécessaire.
Je ne nomme pas mon image puisqu'elle ne sera pas appelée.
Ensuite, les deux lignes servent à récupérer tout ce qui est nécessaire de l'image précédente.
J'aurais pu m'arrêter là, mais ça me permet d'introduire un nouveau concept de Docker.
Le but de Docker est d'exécuter des applications ou des commandes, c'est pas une découverte.
Comme je l'ai dit, le but des microservices est d'avoir une image par fonction ou par commande.
Quel est l'intérêt d'avoir une image dédiée à une commande s'il faut passer le nom de la commande à chaque exécution du container~?
Il n'y en a aucun, nous sommes d'accord.
Docker a donc proposé deux possibilités distinctes et complémentaires d'exécuter une commande par défaut.
Et c'est là que \liglat{ENTRYPOINT} et \liglat{CMD} entrent en jeu.

Les deux peuvent s'appeler exactement de la même façon, c'est la façon dont elles seront traitées qui va changer en fonction du contexte.
Le \liglat{ENTRYPOINT} sera toujours exécuté lors de l'appel du container.
La seule façon de ne pas l'exécuter est de passer spécifiquement une autre valeur à \liglat{ENTRYPOINT} lors de l'appel du container, cette nouvelle valeur sera exécutée à la place de celle définie dans l'image.
Le \liglat{CMD} sera exécuté si rien n'est passé en paramètre lors de l'appel du container.
Si un paramètre est passé, alors ce paramètre remplacera le contenu de \liglat{CMD}.
Si \liglat{ENTRYPOINT} est défini, alors le contenu de \liglat{CMD} lui sera ajouté.

J'aurais pu le faire en une seule ligne, soit avec \liglat{ENTRYPOINT ["curl", "example.com"]}, soit avec \liglat{CMD ["curl", "example.com"]}, ce qui serait revenu au même pour la création de l'image.
La différence aurait été au moment de l'appel du container.
La façon dont j'ai rédigé ces deux lignes est la seule façon qui me permette d'exécuter \liglat{curl} sur n'importe quel site juste en le passant en paramètre.
Nous verrons ça dans un instant.
Pour l'instant, regardons ce que ça donne.
C'est particulièrement verbeux, j'épure.

\begin{listbash}
(multicurl)> docker build -t multicurl .
Sending build context to Docker daemon   2.56kB
Step 1/13 : FROM nixos/nix AS source
latest: Pulling from nixos/nix
...[SNIP]...
Successfully built 39b5112bcf6d
Successfully tagged multicurl:latest
(stef)> docker run --rm multicurl
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  1256  100  1256    0     0   7801      0 --:--:-- --:--:-- --:--:--  7801
<!doctype html>
...[SNIP]...
</div>
</body>
</html>
\end{listbash}

Et ça marche, il a exécuté la commande \liglat{curl example.com} en concaténant le contenu de \liglat{ENTRYPOINT} et de \liglat{CMD} puisque je n'ai passé aucun argument..
Maintenant, je passe un argument pour illustrer l'intérêt des deux lignes.

\begin{listbash}
(stef)>  docker run --rm multicurl free.fr
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   185  100   185    0     0  37000      0 --:--:-- --:--:-- --:--:-- 37000
<html>
<head><title>301 Moved Permanently</title></head>
<body bgcolor="white">
<center><h1>301 Moved Permanently</h1></center>
<hr><center>nginx/1.14.2</center>
</body>
</html>
\end{listbash}

Là, il a exécuté la commande \liglat{curl free.fr} en concaténant le contenu de \liglat{ENTRYPOINT} avec \liglat{free.fr} passé en paramètre et en oubliant le contenu de \liglat{CMD} qui n'est utilisé que par défaut.
Regardons un peu les images.

\begin{listbash}
(stef)> docker images
REPOSITORY        TAG       IMAGE ID       CREATED         SIZE
multicurl         latest    39b5112bcf6d   2 minutes ago   44MB
<none>            <none>    79484ddb363f   2 minutes ago   543MB
nixos/nix         latest    e2f06f685d89   52 years ago    286MB
\end{listbash}

Il a récupéré l'image de nix qui fait 286Mo pour pouvoir créer les autres images.
L'image n'a pas été créée il y a 52 ans.
Le principe est la reproductibilité.
Pour être sûr que toutes les commandes soient identiques, il ne faut pas qu'un changement de date puisse modifier la dérivation.
Donc, par défaut, toutes les dates sont à zéro, c'est à dire au premier janvier 1970.
Pour ceux qui ne savent pas comment ça fonctionne en informatique, les dates sont stockées sous la forme du nombre de secondes depuis le premier janvier 1970.
À partir de cette image, il a installé \liglat{curl} avec toutes les couches utiles pour faire une image de 543Mo.
Et enfin, reste mon image multicurl de 44Mo, ce qui est vraiment petit.
Si vous vous souvenez, avec l'image ubuntu, j'étais descendu un peu sous la barre des 90Mo.
Je précise, au cas où, ces deux grosses images ne sont plus utiles, seule l'image finale est indépendante.
Si l'image est aussi petite, c'est parce que c'est bien géré.
Mais en même temps, si elle est aussi grosse, c'est parce que toutes ses dépendances sont présentes.
L'application est petite, mais elle se base sur wget qui est donc nécessaire, installé et récupéré sans que je n'ai rien à préciser.

Je vais faire pareil avec ping pour des raisons qui vont devenir plus claires par la suite.
Par contre, les explications n'ont pas besoin d'être aussi détaillées.
La seule différence est que ping n'est pas un package isolé mais est inclus dans un package.
Il faut donc installer tout le package, je n'ai pas réussi à isoler ping.
Donc, voici mon image.

\begin{monfichier}{dockerfile}{Dockerfile}
# Image qui servira de support à mon image finale
FROM nixos/nix AS source
RUN mkdir -p /extract/store
RUN nix-env --switch-profile /extract/profile
RUN rm ~/.nix-profile
RUN ln -s /extract/profile ~/.nix-profile
RUN hash -r
# Instalation de ping qui appartient au package iputils
RUN nix-env --profile /extract/profile -i iputils
RUN cp -a $(nix-store -qR /extract/profile) /extract/store/
 
# Image finale qui part d'une image vide et qui sera donc minimale
FROM scratch
COPY --from=source /extract/store /nix/store
COPY --from=source /extract/profile/ /usr/local/
ENTRYPOINT ["ping"]
CMD ["example.com"]
$
\end{monfichier}

Je construis et exécute les commandes pour vérification et vérifie la taille, pareil, j'élague l'inutile.

\begin{listbash}
(multiping)> docker build -t multiping .
Sending build context to Docker daemon   2.56kB
Step 1/13 : FROM nixos/nix AS source
...[SNIP]...
Successfully built 9d2e6bfac250
Successfully tagged multiping:latest
(stef)> docker run --rm multiping
PING example.com (93.184.216.34) 56(84) bytes of data.
64 bytes from 93.184.216.34 (93.184.216.34): icmp_seq=1 ttl=47 time=83.5 ms
64 bytes from 93.184.216.34 (93.184.216.34): icmp_seq=2 ttl=47 time=83.7 ms
(stef)> docker run --rm multiping free.fr
PING free.fr (212.27.48.10) 56(84) bytes of data.
64 bytes from www.free.fr (212.27.48.10): icmp_seq=1 ttl=51 time=1.29 ms
64 bytes from www.free.fr (212.27.48.10): icmp_seq=2 ttl=51 time=1.40 ms
(stef)>  docker images
REPOSITORY        TAG       IMAGE ID       CREATED          SIZE
multiping         latest    9d2e6bfac250   3 minutes ago    33.3MB
<none>            <none>    c4010c54befa   3 minutes ago    465MB
nixos/nix         latest    e2f06f685d89   52 years ago     286MB
\end{listbash}

Et là, pareil, ça fonctionne, c'est super, la taille de 33,3Mo est aussi très petite.
J'ai donc deux images minimales qui sont propres et utilisables n'importe où.
Il a donc été intéressant de surmonter les difficultés à comprendre nix.
Mais ce n'est pas fini, il est possible d'exploiter les capacités de nix.
C'est ce que nous allons voir maintenant.

%end-include

