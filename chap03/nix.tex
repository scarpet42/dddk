%begin-include
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Auteur : Stéphane CARPENTIER
% Fichier : nix.tex
% 	Modif : dim. 05 mars 2023 21:23
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\Section{Présentation rapide de nix}

Initialement, nix désigne plusieurs choses.
C'est d'abord à la fois un gestionnaire de paquets et le langage utilisé pour les gérer.
Vous allez me dire~: encore un gestionnaire de paquets~?
Et c'est exactement ce que je me suis dit la première fois que j'en ai entendu parler.
Mais en fait, ses concepts sont intéressants et viennent pour résoudre beaucoup de problèmes.
Le problème principal dans la distribution d'une application, c'est la gestion des dépendances.

Pour faire simple, avec Windows, la gestion est simplifiée~: l'application vient avec toutes ses librairies.
Donc, quand une application a des librairies partagées avec d'autres applications, les librairies sont installées plusieurs fois.
Pour les montées de versions, c'est simple, la nouvelle version de la librairie est installée.
Donc, au fil des mois et des années, il y a beaucoup de versions différentes de la même librairie installées dans beaucoup d'endroits.
Et quand plusieurs applications partageant des librairies sont lancées en même temps, la même librairie est chargée plusieurs fois en mémoire.
Ça fonctionne, c'est simple, mais c'est un gaspillage des ressources.

Avec Linux, la gestion des ressources est considérablement améliorée.
En échange d'une plus grande complexité.
Les librairies ne sont pas déployées dans le répertoire personnel de l'application mais dans un répertoire partagé.
Des difficultés vont se poser lorsque deux applications ont besoin de la même librairie dans des versions différentes.
Au fil des mois et des années, il devient difficile de savoir si quelque chose a été installé parce que c'était requis par le système ou si c'est sur demande de l'utilisateur.
Et si l'utilisateur sait qu'il l'a installé lui-même, il ne sait pas si c'est devenu requis par le système depuis son installation.
Dit autrement, à part tout réinstaller de zéro, il est difficile de faire du ménage sereinement.

En bref, les deux ont des avantages et des inconvénients mais il y a des problèmes à l'arrivée.
Arrive nix.
Dont la philosophie est fondamentalement différente.
Pour commencer, nix est le nom d'un gestionnaire de paquets.
Lorsqu'un paquet est installé, tous ses composants et toutes ses dépendances sont installées dans le répertoire central \liglat{/nix/store} avec pleins de liens symboliques dans tous les sens.
Et le nom de chaque composant est un nom unique prenant en compte tout ce qui est nécessaire pour le générer.
Quand j'écris tout, ça veut vraiment dire beaucoup, chez moi je l'ai installé juste pour voir avec presque rien d'installé en plus.
Et déjà, quand je regarde ce qu'il y a dedans, je vois plus de 11000 fichiers~:

\begin{listbash}
(stef)> ls /nix/store/ | wc -l
11429
(stef)> ls /nix/store/ | head
000swl22ml1ir4mq5qaz05c4v1smzd3i-perl5.34.0-Encode-Locale-1.05.drv
000w2b8w9mrzw8i15ak7cdqcdbayfv6q-xcpdftips.r50449.tar.xz.drv
001gp43bjqzx60cg345n2slzg7131za8-nix-nss-open-files.patch
001j6m8z5yv9lm11xgadf0024pw6yjf5-texlive-addfont-1.1.drv
002gbsl500p1b9m4wlinazna58mcmn6z-gnum4-1.4.19.drv
004jhqgrmgn602skc5wwlyhfd81g9cnn-metre.r18489.tar.xz.drv
0073l70p3xqa2x30ivw56l8560lisdlj-urwchancal.r21701.tar.xz.drv
008x5pzs0xvjz8xv6qaa7zvmlawqc11n-setlocale-1.0.0.10.drv
009cyfrfs6d9gd535556jvpvwx9wfv8w-CVE-2019-13232-1.patch.drv
00c1v4vmiaz81j84p0z81jawdsbbrky2-pst-labo.r39077.tar.xz.drv
\end{listbash}

C'est pas fait pour être lu par un être humain.
Tous les fichiers ont un numéro de hash, un nom, un numéro de version et éventuellement une extension.
L'extension \liglat{.drv}, c'est pour «~dérivation~», c'est le fichier final.
Le hash prend tout en compte et garanti l'unicité du fichier résultant.
S'il y a la moindre différence dans les logiciels utilisés, que ce soit la version ou une option du compilateur, ou d'une librairie, le résultat est le même~: le hash est différent.
L'inconvénient du hash dans le nom du fichier, c'est que ce n'est pas pratique à trier par ordre alphabétique.
L'avantage, c'est que deux fichiers de la même librairie avec de petites différences peuvent cohabiter sans difficulté.

Pour faire ça, il utilise nix, qui est aussi le nom d'un langage fonctionnel, pur et feignant.
Pour langage fonctionnel, ça vient du langage de programmation LISP\footnote{Les puristes me diront que nix vient plutôt de haskel, mais haskel est inspiré du lisp}, c'est à dire que tout est basé sur les fonctions.
Les langages de programmation fonctionnelle donnent des programmes qui sont particulièrement configurables puisqu'il n'y a pas de différence fondamentale entre le langage de programmation, le code de l'application et les données de l'application.
Pour le côté pur, c'est pour dire que tout ce qui est utilisé doit être passé à la fonction~: il n'y a pas d'effet de bord, il n'y a pas de variable globale ou de variable d'environnement.
La fonction prend des paramètres en entrée et renvoie un résultat~:
\begin{tuxliste}
\item elle n'utilise rien d'autre que le code et les paramètres qui lui sont passés,
\item elle ne modifie rien qui ait un impact sur le reste du système,
\item la même fonction appelée avec les mêmes paramètres aura donc toujours le même résultat~: c'est ce qui est appelé idempotent.
\end{tuxliste}
Par exemple, si vous avez un programme qui supprime un fichier, le résultat ne sera pas forcément identique s'il est appelé deux fois de suite puisque si le fichier existe avant son premier appel, il n'existe plus lors du second appel.
Ou alors, si vous avez un programme qui ajoute des lignes dans un fichier, lors de trois appels successifs, les lignes pourront être présentes en triple.
Pour le côté feignant, c'est pour dire que les évaluations ne sont exécutées que quand elles sont nécessaires.
Un peu comme quand vous faites un test \liglat{si a && b alors...} où le \liglat{b} n'est exécuté que si le \liglat{a} est vrai.
Le premier intérêt est d'économiser des ressources en n'exécutant que ce qui est utile.
Le second intérêt est que \liglat{a} et \liglat{b} peuvent être définis plus tard~: il ne sont utiles qu'au moment de leurs exécutions.

Donc, la «~dérivation~», c'est le fichier unique qui est construit avec toutes ses dépendances.
Donc, à partir d'un fichier quelconque, il est possible de savoir tout ce qu'il nécessite pour être utilisable.
C'est vraiment la chose la plus importante, et quelque part, la seule à retenir pour la suite.
Contrairement aux autres gestionnaires de paquets, dans lesquels les dépendances sont décrites par le mainteneur, ici elles sont déduites par construction.
Et c'est très important, car c'est cette propriété qui va nous permettre de construire des images de docker de façon non vues jusqu'ici.

Petite précision pour éviter les confusions ultérieures~: nixos c'est le système d'exploitation basé sur nix.
Le principe a été de considérer que les fichiers de configuration du système pouvaient être gérés de la même façon que les applications.
Comme une dérivation garantit d'avoir toujours exactement le même comportement, un déploiement de nixos sera toujours identique.
Il n'y a pas de variable d'environnement pour apporter du suspense.
Je ne devrais pas en avoir besoin par la suite, mais par souci d'exhaustivité, nixpkgs est l'endroit où sont gérés tous les packets utilisés par nix.

%end-include

