%begin-include
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Auteur : Stéphane CARPENTIER
% Fichier : nodejs.tex
% 	Modif : dim. 07 janv. 2024 10:07
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\Section{La nature a horreur du vide}

C'est bien beau d'avoir un cluster Kubernetes à moi tout seul qui tourne.
J'ai beau avoir un superbe dashboard, il se sent quand même un peu seul au monde.
Il faut que je lui donne de la compagnie.
Comme je l'ai indiqué, pour comprendre Kubernetes, il faut comprendre Docker.
C'est parce que Kubernetes fait tourner des containers.
Il faut donc que je donne une image à Kubernetes pour avoir un container qui puisse être monitoré par mon dashboard.

Puisque Docker est bien maîtrisé, je peux commencer à définir un autre terme.
Le \Definition{c}{pod}{plus petite unité utilisable par Kubernetes, permet à Kubernetes de s'affranchir de la technologie du container, que ce soit Docker ou un concurrent}.
C'est dans un pod que le container est déployé.
Il est possible de faire tourner plusieurs containers dans le même pod.
Dans ce cas, les containers partagent la même adresse IP visible de Kubernetes et peuvent communiquer entre eux en utilisant \liglat{localhost}.
Cependant, je n'en ai pas compris l'intérêt~: je n'ai pas vu d'exemple concret pour me l'expliquer et je n'en ai pas vraiment cherché non plus.

Si vous avez lu mon second chapitre, vous avez vu que j'ai ma propre image.
Mais si j'ai rédigé ce chapitre, qui se voulait intermédiaire, c'est parce que je ne peux pas utiliser cette image avec Kubernetes.
Ça semble évident~: mon container n'est pas un serveur qui tourne en attendant d'être sollicité.
C'est une application qui tourne à la demande.
Je ne vais pas faire tourner un compilateur à vide 24h/24 en attendant qu'un document soit déposé pour pouvoir le compiler.
Non seulement ce serait compliqué à mettre en œuvre et à utiliser, mais en plus ce serait un gaspillage considérable de ressources.

J'ai vraiment besoin d'une autre image.
C'est donc à ça que servait la section intermédiaire précédente.
Et c'est donc là qu'entre en jeu l'image que j'ai créée juste avant.
C'est parti.

\begin{listbash}
(stef)> kubectl create deployment monweb --image=scarpet42/helloweb
deployment.apps/monweb created
\end{listbash}

La commande n'est pas très compliquée à comprendre.
Je crée un déploiement qui s'appelle \liglat{monweb} et qui utilise mon image en allant la chercher sur dockerhub.
Et donc, maintenant, je regarde le résultat de la création de mon déploiement.

\begin{listbash}
(stef)> kubectl get all
NAME                       READY   STATUS    RESTARTS   AGE
pod/monweb-f97c767-48gtm   1/1     Running   0          2m47s

NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   11d

NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/monweb   1/1     1            1           2m47s

NAME                             DESIRED   CURRENT   READY   AGE
replicaset.apps/monweb-f97c767   1         1         1       2m47s
\end{listbash}

Là, il y a plein de nouveautés.
Le second bloc qui contient l'adresse IP était déjà présent.
Par contre, les trois autres blocs sont nouveaux.
J'ai installé un serveur web~: pour y accéder, j'ai forcément besoin d'une IP et d'un port.
Or, la seule IP est une IP privée qui n'est pas sur mon sous-réseau mais qui est sur le sous-réseau de ma VM.
C'est donc une IP interne au cluster et je n'ai aucun moyen d'y accéder.
C'est la base du routage réseau~: les IP privées ne sont pas routables.
Ce n'est pas une spécificité de Kubernetes contournable par une ruse de cowboy.
Fin de l'histoire.
J'ai donc installé un site web pour tenir compagnie à mon dashboard mais je ne peux pas y accéder.
C'est triste.
Est-ce vraiment définitif~?

J'ai besoin de demander un \Definition{c}{service}{une abstraction qui permet de définir un ensemble de pods et la façon d'y accéder}.
La façon d'y accéder étant le réseau bien sûr, mais il est possible d'être plus spécifique.
Le principe du pod, c'est de rendre utilisable des containers par Kubernetes.
Or, un container, comme déjà dit est un composant jetable.
Donc, l'utilisation d'un pod dédié au container fait que le pod est jetable de la même façon.
Ce qui veut dire qu'à chaque suppression et création de pod, il y a un changement d'adresse IP.
Ce qui peut devenir problématique pour son utilisation.

Le but du service est donc d'exposer un nom et une IP stables à Kubernetes et de le laisser s'occuper de la gestion des variations d'IP internes des pods.
C'est de là que vient le terme \Definition{c}{micro-services}{services exposant chacun un pod chargé d'une unique tâche} que j'ai déjà défini de façon un peu différente plus haut avant d'avoir la notion de services à ma disposition.
Même si la définition change un peu, le concept reste le même.

Il y a quatre types de services, je ne vais pas tous les regarder.
Celui qui est créé par défaut, \Definition{c}{ClusterIP}{expose le service sur une adresse IP interne joignable seulement depuis l'intérieur du cluster ou un node}.
Le service \liglat{kubernetes} créé par minikube est de ce type, il est donc fait par Kubernetes pour Kubernetes.
C'est à dire que le principe n'est pas d'y toucher mais de créer le nôtre.

Le type de service le plus simple, c'est le type NodePort, mais comme je ne sais pas comment le définir, je vais l'utiliser.
En vous montrant comment je m'y prends.
Ensuite, quand son utilisation sera plus claire, je pourrai le définir plus facilement.
Pour créer un service NodePort, rien de plus simple.
Comme d'habitude, j'utilise la commande \liglat{kubectl}, je lui dit que je veux créer un \liglat{service} de type \liglat{NodePort}, sur le déploiement \liglat{monweb} et que je veux mapper le port \liglat{8080} de mon pod sur son port \liglat{8080}.

\begin{listbash}
(stef)> kubectl create service nodeport monweb --tcp=8080:8080
service/monweb exposed
(stef)> kubectl get services
NAME         TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
kubernetes   ClusterIP   10.96.0.1        <none>        443/TCP          11d
monweb       NodePort    10.102.211.227   <none>        8080:31899/TCP   2m35s
\end{listbash}

C'est tout.
Et maintenant~?
Je vois bien qu'il a un port \liglat{31899} qui m'est proposé et qui est mappé sur le port interne du service.
Mais toujours rien quant à une IP publique.
Regardons plus en détail les deux services.
Oui, les deux, il faut aussi analyser l'ancien pour comprendre.

\begin{listbash}
(stef)> kubectl describe services
Name:              kubernetes
Namespace:         default
Labels:            component=apiserver
                   provider=kubernetes
Annotations:       <none>
Selector:          <none>
Type:              ClusterIP
IP Families:       <none>
IP:                10.96.0.1
IPs:               10.96.0.1
Port:              https  443/TCP
TargetPort:        8443/TCP
Endpoints:         192.168.39.109:8443
Session Affinity:  None
Events:            <none>


Name:                     monweb
Namespace:                default
Labels:                   app=monweb
Annotations:              <none>
Selector:                 app=monweb
Type:                     NodePort
IP Families:              <none>
IP:                       10.102.211.227
IPs:                      10.102.211.227
Port:                     <unset>  8080/TCP
TargetPort:               8080/TCP
NodePort:                 <unset>  31899/TCP
Endpoints:                172.17.0.5:8080
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>
\end{listbash}

Là, vous voyez 4 IP, toutes privées.
Gars mauvais, fin de partie, je dois rentrer chez môman~?
Pas tout de suite, il y a une subtilité.
Sur toutes les IP, il y en a une qui est sur le même réseau que mon ordinateur.
Je peux donc y accéder, c'est pour ça que l'IP du cluster est indiquée \liglat{Endpoints}, c'est \liglat{192.168.39.109}, c'est prévu pour.
Bon, ça, c'est pour l'IP du cluster, mais moi, ce que je veux, c'est tapper sur mon pod.
Sur le service \liglat{monweb}, il y a un \liglat{Endpoints} avec une autre IP privée et le port \liglat{8080} sur lequel écoute mon serveur web.
C'est quoi cette IP~?
C'est celle de mon pod~?

\begin{listbash}
(stef)> kubectl describe pod monweb-f97c767-48gtm | grep IP
IP:           172.17.0.5
IPs:
  IP:           172.17.0.5
\end{listbash}

Oui, ça semble être la bonne piste.
Donc, si je comprends bien, le service écoute sur le port \liglat{31899} du cluster.
Puis, le service transfère le flux sur le port \liglat{8080} du pod, qui le transfère à son tour sur sur le port \liglat{8080} du container qu'il héberge.
Donc, normalement, il suffit que je contacte le port \liglat{31899} sur l'IP du cluster pour voir ma page web.

\begin{listbash}
(stef)> curl 192.168.39.109:31899
Bonjour le monde depuis monweb-f97c767-48gtm
\end{listbash}

BINGO.
J'ai gagné.
Champagne.
Bon, en fait, je ne suis pas un fan de champagne, je peux en boire, mais je préfère à peu près n'importe quoi au champagne.
Je vais donc boire autre chose pour fêter ça.

Je vous l'ai faite courte, mais honnêtement, j'ai mangé grave pour réussir à afficher juste cette pauvre ligne avec le hostname dedans.
J'ai pas suivi 4242 heures de formations et les tutos, sont plutôt légers et si je passe du temps à écrire ce document c'est parce que je n'ai rien trouvé d'intermédiaire.
Il y a quelques mois, je ne connaissais ni Kubernetes, ni Docker, ni nodejs.
Il m'a fallu lire et faire des jeux d'essais/erreurs avant d'arriver enfin au Graal qui consiste à avoir un site Internet personnel qui tourne sur Kubernetes.

Donc, je peux définir le type de service le plus simple, le \Definition{c}{NodePort}{relie un port interne avec un port externe pour exposer le port interne vers l'extérieur du cluster}.
Normalement, avec tout ce que j'ai montré avant, ça doit être globalement compréhensible.
Le principe, c'est que vous avez~:
\begin{tuxliste}
\item un serveur qui tourne dans un container et qui écoute sur un port particulier,
\item un pod qui fait tourner le container et qui expose son port avec une adresse IP quelconque,
\item un cluster avec une IP publique,
\item un service NodePort qui relie le port du pod vers le port public du cluster.
\end{tuxliste}
Et lorsque vous appelez l'IP publique de votre cluster sur le port choisi, il vous retourne le résultat de l'exécution du pod.
Tout ça pour faire tourner un pod dont je n'ai plus besoin.
Je peux le supprimer.

\begin{listbash}
(stef)> kubectl delete pod monweb-f97c767-48gtm
pod "monweb-f97c767-48gtm" deleted
\end{listbash}

Pour rire, je vais relancer ma commande pour voir l'erreur que j'obtiens en cherchant un service web qui n'existe plus.

\begin{listbash}
(stef)> curl 192.168.39.109:31899
Bonjour le monde depuis monweb-f97c767-ml2w4
\end{listbash}

Mais, c'est pas une erreur ça.
Vous avez vu~?
Il est toujours disponible après suppression.
Ou presque, car les 5 derniers caractères du nom du pod ne sont plus les mêmes.

\begin{listbash}
(stef)> kubectl get all
NAME                       READY   STATUS    RESTARTS   AGE
pod/monweb-f97c767-ml2w4   1/1     Running   0          4m30s

NAME                 TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
service/kubernetes   ClusterIP   10.96.0.1        <none>        443/TCP          11d
service/monweb       NodePort    10.102.211.227   <none>        8080:31899/TCP   67m

NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/monweb   1/1     1            1           89m

NAME                             DESIRED   CURRENT   READY   AGE
replicaset.apps/monweb-f97c767   1         1         1       89m
\end{listbash}

Quand j'ai créé mon déploiement, je lui ai dit que je voulais un pod.
Il a créé mon pod tout seul comme un grand.
Et quand j'ai supprimé mon pod, il l'a vu et il a remarqué que ce n'était plus conforme à mon déploiement.
Et il a donc recréé un pod.
Dire que Kubernetes permet de faire de la haute disponibilité n'est donc pas une légende.
Mais comment ça marche~?
Pour mieux analyser les blocs ci-dessus, c'est mieux d'avoir autre chose que des \liglat{1} partout.
Je vais donc à la fois changer mon déploiement et m'y prendre un peu mieux.

\begin{listbash}
(stef)> kubectl delete deployments.apps monweb
deployment.apps "monweb" deleted
(stef)> curl 192.168.39.109:31899
curl: (7) Failed to connect to 192.168.39.109 port 31899: Connexion refusée
(stef)> kubectl delete service monweb
service "monweb" deleted
(stef)> kubectl get all
NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   11d
\end{listbash}

Voilà, je n'ai plus rien, je peux recommencer.
Vous avez vu, tout ce que j'ai fait, c'est utiliser un programme défini et stocké ailleurs.
Juste un, avec plusieurs lignes de commandes plus ou moins simples pour dire ce que je voulais.
Mais si en fait, je voulais recommencer et faire exactement ce que j'avais avant de tout supprimer~?
Il faudrait que je tape les mêmes lignes de commandes, avec les mêmes paramètres et éventuellement dans le même ordre.
J'ai bien l'historique de ma ligne de commande pour m'aider.
Mais quand j'ai fait plusieurs tests, retrouver celui qui fonctionnait le mieux n'est pas forcément instantané.
Ou alors, si je ne suis plus sur mon ordinateur et que je veux reproduire la même chose.
Et là, je ne parle que d'un serveur web isolé.
Il y a donc mieux et c'est ce que nous allons voir maintenant.

%end-include

