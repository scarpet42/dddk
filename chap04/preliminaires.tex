%begin-include
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Auteur : Stéphane CARPENTIER
% Fichier : preliminaires.tex
% 	Modif : dim. 07 janv. 2024 09:54
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\Section{À quoi ça sert Kubernetes \texorpdfstring{?}{\?}}

Et voilà, enfin, le chapitre, tant attendu, annoncé par le titre de ce document.
Par contre, là, je vais moins m'investir que pour comprendre Docker.
La raison est que je n'ai pas de raison de m'y intéresser de façon personnelle.
Je ne vais m'y intéresser que pour des raisons professionnelles et je n'aurai donc pas la motivation de chercher à vraiment approfondir.

Reprenons la métaphore à l'endroit où je l'avais laissée à la fin du premier chapitre.
Comme je le disais, la normalisation des containers a fait gagner du temps et de l'argent à toute l'industrie maritime.
Maintenant, en utilisant un bateau, une grue ou un camion destiné à déplacer un container, aucune question ne se pose.
La grue est capable de sortir le container du camion et de le déposer sur le bateau très facilement.

Donc, c'est une bonne chose vue du grutier, du conducteur du camion ou du pilote du bateau.
Et comme chacun est plus efficace dans l'exécution de son métier, à l'échelle internationale de l'argent est gagné.
Maintenant, si le but est de faire du commerce à grande échelle, il va y avoir besoin d'une gestion sérieuse.

Vous avez besoin de savoir sur quels bateaux sont quels containers.
Mais aussi où sont vos bateaux.
Et aussi sur quels bateaux vous pouvez encore ajouter des containers.
Et aussi où vont vos bateaux.
Parce que vous devez éviter de mettre un container sur un bateau qui a de la place alors que les deux vont dans des endroits opposés.
Et aussi de savoir quels containers et quels bateaux ont besoin d'entretien.
Et aussi plein d'autres choses que je ne connais pas parce que ce n'est pas mon métier.

Bref, quand le but est de gérer quelques containers et quelques bateaux, c'est facile.
Quand le but est de passer à la gestion des bateaux et containers à grande échelle, les choses se corsent.
Avec l'informatique, c'est exactement pareil.
Si vous avez quelques containers à gérer, un peu comme moi avec mon image de \LaTeX{}, c'est facile.
Surtout si le container ne tourne qu'à la demande.

Par contre, si vous voulez gérer plein de containers qui doivent être hautement disponibles, les choses sont plus complexes.
Vous avez des machines sur lesquelles tournent des containers.
Et vous devez suivre la disponibilité des containers.
Et vous devez vous assurer que vos machines ne sont pas surchargées.
Et si vous voulez lancer un autre container, il faut vous assurer qu'il est mis sur la machine la moins chargée.
En vous assurant qu'il peut bien contacter les containers avec lesquels il doit interagir pour des raisons opérationnelles.
En vous assurant qu'il ne peut pas contacter ceux avec lesquels il ne doit pas interagir pour des raisons de sécurité.

Et si vous avez beaucoup de containers, ça peut être délicat à gérer.
Et si vous avez compris le principe des micro-services dont j'ai déjà parlé, vous vous êtes organisés pour avoir des petits containers dédiés à une tâche précise.
Et c'est donc là que Kubernetes entre en jeu.
Vous lui dites ce que vous voulez faire tourner et dans quelles conditions.
Et lui, il le fait~: il s'assure que ça tourne, il s'assure que les ressources sont bien optimisées, il s'assure que les défauts sont corrigés, il s'assure que le café est servi, ah non, je m'égare.

Bref, vous ne le gérez pas.
Vous lui dites ce que vous voulez et lui il se gère tout seul pour répondre le mieux possible à vos désirs.
C'est magique~?
Non, c'est technique.
C'est très technique.
Je dirais même plus~: c'est très technique.
Tout votre code est dans des containers.
Et vous lui dites comments vous voulez utiliser ces containers.

La première difficulté est de comprendre les concepts utilisés par Kubernetes pour s'en sortir.
Vous avez d'un côté des machines physiques.
Si vous dites que maintenant vous avez des VM, je dirai que c'est pareil dans le cas qui nous intéresse.
D'un côté, vos VM mettent des ressources à disposition (RAM, CPU et autre) qui sont situées sur des machines physiques.
D'un autre côté, vos VM interagissent entre elles de la même façon que le feraient des machines physiques.
Donc, d'une façon conceptuelle dans le cas qui nous intéresse, VM et machine physique c'est pareil et vous allez arrêter de m'interrompre pour ça.

D'ailleurs c'est tellement la même chose que pour Kubernetes il y a un concept que je vais définir dès maintenant.
C'est le \Definition{c}{node}{une machine physique ou une machine virtuelle}.
Mais dans la présentation des concepts de Kubernetes c'est mieux de parler de machine physique.
Ça permet de mieux visualiser la différence entre la représentation logique de Kubernetes de la séparation physique de vos machines.
J'utiliserai le terme node quand ce sera plus adapté au contexte.
Maintenant, merci de me laisser reprendre où j'en étais avant de me faire faire une digression oiseuse.

Vous avez d'un côté les machines physiques.
Avec des resources limitées.
Même si elles peuvent être bien plus importantes que ce dont vous avez besoin, elles sont quand même limitées.
Ne venez pas me dire que c'est parce que je ne parle pas de VM et qu'avec une VM vous augmentez les ressources en deux clics de souris~: une fois que vous avez augmenté votre VM pour utiliser toutes les ressources disponibles sur la machine vous vous retrouvez dans la situation précédente.
Et avec des connexion entre vos machines physiques pour qu'elles puissent échanger.

Et de l'autre côté, vous avez vos containers.
Qui doivent interagir entre eux.
Et vous, vous dites à Kubernetes comment ils doivent interagir.
Et c'est à Kubernetes de se débrouiller pour déployer les containers en optimisant les ressources.
En s'assurant que si un container est supprimé d'une machine physique pour être déployé sur une autre machine physique, il continue à avoir les mêmes interactions qu'il avait avec les autres containers.
Et c'est bien parce que Kubernetes masque la réalité physique pour vous laisser gérer l'organisation logique.
Dit autrement, vous vous occupez de dire à Kubernetes de gérer des nodes et lui, il traduit ça en ressources physiques de façon transparente pour vous.

Enfin presque.
Il ne masque pas tout parce qu'il faut pouvoir l'aider un peu.
C'est pour ça que la notion de node est toujours présente.
Par exemple, il y a des ressources critiques et il ne faudrait pas qu'un de vos containers empêche Kubernetes de faire son rôle de gestion des ressources.
Il faut s'assurer que Kubernetes a toujours assez de ressources pour pouvoir contrôler un container qui deviendrait fou.
C'est donc ça qui est rigolo.

Donc, maintenant, quand vous entendez dire que Kubernetes est un orchestrateur, vous comprenez ce que ça signifie.
Ça signifie toute la gestion des containers.
Et voilà, maintenant vous en savez autant que moi.
Non, je rigole, j'ai d'autres choses à raconter.
Mais sérieusement, je ne sais pas trop par quel bout le prendre.
Je vais donc faire comme pour Docker~: je vais commencer par la ligne de commande et je vais voir où ça va me mener.
Autant, pour Docker, j'avais une idée de l'ordre de ce que j'allais rédiger.
Autant, là, j'ai une idée de certains des sujets que je dois aborder, mais je n'ai aucune idée de la façon dont je vais en parler.
J'ai procrastiné autant que j'ai pu, maintenant, c'est parti, je plonge.
Sans gilet de sauvetage.

%end-include

