%begin-include
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Auteur : Stéphane CARPENTIER
% Fichier : fichiers.tex
% 	Modif : dim. 07 janv. 2024 10:16
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\Section{Allons plus loin avec la presque bonne méthode}

D'abord, pour faire simple, il y a trois méthodes de gestion.
La première, c'est celle que j'ai employée jusqu'à maintenant.
La \Definition{c}{commande impérative}{consiste à exécuter les commandes directement sur les objets}.
C'est ce que j'ai fait jusqu'à maintenant, quand j'ai créé des services et des déploiements.
C'est pratique, c'est rapide, c'est facile pour découvrir et faire des tests.
Par contre, pour la pérennité c'est moins glorieux et c'est pour ça que je disais que kubectl est fait pour ne pas être utilisé.

La seconde méthode, la \Definition{c}{configuration impérative}{consiste à demander à Kubernetes de faire une action particulière avec un fichier de configuration}.
Par exemple, si j'ai un fichier de configuration pour un service, je lui dit de créer ou de supprimer le service défini dans le fichier de configuration.
Je peux lui dire la même chose avec un ensemble de fichiers.
C'est pour la pérennité, c'est une avancée considérable par rapport à la ligne de commande.
En contrepartie, il faut savoir comment écrire tout son fichier.
Pour découvrir Kubernetes, c'est délicat.

La bonne méthode, la \Definition{c}{configuration déclarative}{consiste à demander à modifier l'état du cluster pour atteindre l'état souhaité}.
C'est à dire que vous avez les mêmes fichiers de configuration que dans la méthode précédente.
Mais au lieu de demander à Kubernetes de faire une action précise avec ces fichiers, vous lui demandez de regarder les différences entre ce qui tourne et ce que vous voulez.
Puis Kubernetes fait tout ce qu'il faut pour vous satisfaire.
Dit comme ça, ça à l'air cool.
Il ne fait pas le café, même pas le triple déca ni trois simples décas, il se contente de gérer le cluster ce qui n'est déjà pas si mal.
Sauf que c'est plus compliqué à mettre en place et comme c'est très récent, c'est moins mature.
Il paraît que c'est la méthode qui marche le mieux pour des gros projets de production avec beaucoup de fichiers de configuration et de sous-répertoires.
Ce qui se conçoit aisément~: au lieu de tout supprimer et de tout recréer à chaque déploiement, seules les modifications nécessaires sont appliquées.
En échange, les impacts de l'utilisation de cette méthode peuvent être plus complexes à appréhender.

Les trois méthodes ne sont pas exclusives bien sûr.
Il faut juste avoir conscience que la combinaison des méthodes peut avoir des impacts à terme.
Je dis ça parce que le mieux est de choisir sa méthode sans être trop intégriste sur le choix de la méthode.
Par exemple, vous pouvez être fiers d'avoir réussi à mettre en place un super environnement utilisant la méthode de configuration déclarative.
Le mieux est donc de continuer sur votre lancée en utilisant une chaine de CI/CD pour faire évoluer votre environnement.
Sauf que le jour où vous êtes confronté à une intrusion ou à un incident de prod non géré par Kubernetes, ça change la donne.
Vous n'avez pas le temps de modifier votre configuration en espérant que sa prise en compte par Kubernetes va être celle attendue.
Non, là, vous devez utiliser la ligne de commande impérative pour corriger et protéger.
Ensuite, vous voyez comment intégrer vos amélioration dans votre chaine de CI/CD.
Les trois méthodes existent pour de bonnes raisons, il faut les connaître.

La bonne méthode est de rédiger un fichier dans lequel tout est décrit.
Initialement, c'est moins instantané d'écrire un gros fichier qui contient tout que d'écrire les commandes une par une.
Par contre, ensuite, une fois le fichier rédigé, tout devient plus simple.
Le fichier peut être
\begin{tuxliste}
\item déployé n'importe où avec le même résultat.
\item lu pour comprendre ce qui a été déployé.
\item géré par un gestionnaire de versions comme git pour simplifier les retours arrière.
\item géré par un gestionnaire de versions comme git pour conserver un historique des évolutions.
\item édité pour mettre en place des structures beaucoup plus complexes que ce qui peut être fait en ligne de commande.
\end{tuxliste}

Comme je vous l'ai dit, je n'ai besoin que de petits fichiers simples de façon temporaire en guise de découverte.
Je ne vais donc pas m'étendre dans l'analyse de la bonne méthode.
Pour mon utilité, la méthode de la configuration impérative est suffisante.
La complexité engendrée par la configuration déclarative n'apporte pas de meilleure compréhension du sujet.
Mais maintenant que j'ai choisis ma méthode, il faut que je crée mes fichiers de configuration.

Kubernetes reconnait deux formats de fichiers~: yaml et json.
Je trouve franchement json plus lourd et moins lisible que yaml, surtout quand il y a des structures complexes et imbriquées.
J'utilise déjà yaml pour mes déploiements sur gitlab.
La quasi totalité des documents montrent des exemples en yaml et pas en json.
Je ne vais donc pas regarder le json et rester sur yaml.

Pour le contenu du fichier, je vais faire comme pour les commandes.
Je ne vais pas créer de pod, mais directement des déploiements.
Ça fait partie des difficultés~: il y a encore beaucoup de documentations qui expliquent comment créer des pods.
Cependant, il y a quelques mois, la méthode était deprecated en ligne de commande et aujourd'hui elle a été enlevée des versions récentes.
Il est toujours possible de créer directement des pods, mais en utilisant des contournements et ce n'est pas une bonne pratique.
Vous avez vu l'intérêt de créer un déploiement, c'est que le pod est ensuite créé et managé proprement par Kubernetes en cas de problème.
Voilà mon beau fichier.

\begin{monfichier}{yaml}{helloweb.yaml}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mon-deployment
spec:
  replicas: 5
  selector:
    matchLabels:
      monhello: helloweb
  template:
    metadata:
      labels:
        monhello: helloweb
    spec:
      containers:
        - name: moncontainer
          image: scarpet42/helloweb
          ports:
            - containerPort: 8080
---
apiVersion: v1
kind: Service
metadata:
  name: mon-service
spec:
  type: NodePort
  selector:
    monhello: helloweb
  ports:
    - port: 8080
      targetPort: 8080
      nodePort: 30666
\end{monfichier}

Je ne vais pas commencer à avoir une démarche orthodoxe vers la fin du document.
Avant de l'analyser, regardons ce que ça donne pour avoir la surprise.

\begin{listbash}
(helloweb)> kubectl create -f helloweb.yaml
deployment.apps/mon-deployment created
service/mon-service created
(stef)> kubectl get all
NAME                                  READY   STATUS    RESTARTS   AGE
pod/mon-deployment-5d5cdd8798-hbc78   1/1     Running   0          18m
pod/mon-deployment-5d5cdd8798-lzgnj   1/1     Running   0          18m
pod/mon-deployment-5d5cdd8798-skhfw   1/1     Running   0          18m
pod/mon-deployment-5d5cdd8798-sltc2   1/1     Running   0          18m
pod/mon-deployment-5d5cdd8798-zk8xv   1/1     Running   0          18m

NAME                  TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
service/kubernetes    ClusterIP   10.96.0.1       <none>        443/TCP          12d
service/mon-service   NodePort    10.111.79.193   <none>        8080:30666/TCP   18m

NAME                             READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/mon-deployment   5/5     5            5           18m

NAME                                        DESIRED   CURRENT   READY   AGE
replicaset.apps/mon-deployment-5d5cdd8798   5         5         5       18m
(stef)> curl 192.168.39.109:30666
Bonjour le monde depuis mon-deployment-5d5cdd8798-sltc2
(stef)> curl 192.168.39.109:30666
Bonjour le monde depuis mon-deployment-5d5cdd8798-sltc2
(stef)> curl 192.168.39.109:30666
Bonjour le monde depuis mon-deployment-5d5cdd8798-hbc78
(stef)> curl 192.168.39.109:30666
Bonjour le monde depuis mon-deployment-5d5cdd8798-lzgnj
(stef)> curl 192.168.39.109:30666
Bonjour le monde depuis mon-deployment-5d5cdd8798-sltc2
(stef)> curl 192.168.39.109:30666
Bonjour le monde depuis mon-deployment-5d5cdd8798-skhfw
(stef)> curl 192.168.39.109:30666
Bonjour le monde depuis mon-deployment-5d5cdd8798-hbc78
(stef)> curl 192.168.39.109:30666
Bonjour le monde depuis mon-deployment-5d5cdd8798-skhfw
(stef)> curl 192.168.39.109:30666
Bonjour le monde depuis mon-deployment-5d5cdd8798-zk8xv
\end{listbash}

Bon, je vais m'arrêter là, parce que ça suffit.
Je commence par la conclusion~: est-ce que vous n'êtes pas un peu impressionnés~?
Si vous ne savez pas pourquoi vous devriez être impressionnés, je vais donner un peu plus de détails.
Vous avez vu, j'ai juste déployé un fichier de configuration d'une trentaine de lignes.
Si j'ai eu du mal à savoir comment générer ce fichier, c'est parce que je découvre le sujet.
Mais pour un expert, ce fichier est d'une banalité déconcertante.
Vous voyez que j'ai cinq pods, donc cinq containers de la même image qui tournent en même temps.
Avec Kubernetes qui les appelle un par un dans un ordre déterminé par lui-même à chaque fois que je veux avoir la page web.
Ça s'appelle du load balancing.
Et sécurisé puisque vous avez vu que s'il y en a un qui s'arrête Kubernetes le relance tout seul comme un grand.
Sans même avoir à claquer des doigts.

Je repose ma question~: vous n'êtes pas un peu impressionnés~?
Dire que Kubernetes est un orchestrateur, c'est pas juste une façon de parler, ça marche pour de vrai.
Vous allez me dire que d'avoir du load balancing sur un ordinateur unique c'est inutile.
Et vous aurez raison.
Sauf que si vous avez un cluster de plusieurs machines physiques, le fichier de configuration serait exactement le même.
C'est Kubernetes qui se charge de savoir où déployer vos pods et lesquels appeler.

Maintenant, je vais expliquer les blocs du résultat de la commande \liglat{kubectl get all} pour voir ce que j'ai obtenu.
D'abord, le \Definition{c}{déploiement}{moyen déclaratif de décrire ce que je souhaite}.
Même si j'ai utilisé une commande impérative, c'est pas si mal parce que je n'ai fait que définir ce que je voulais en le laissant tout mettre en place.
J'ai dit que je voulais cinq containers, j'en ai actuellement cinq qui sont prêts, et ils sont tous disponibles.

Une information très intéressante est qu'ils sont tous à jour.
Le truc, c'est que si vous faites une montée de version, vous pouvez ne faire votre montée de version que sur un nombre réduit de containers.
Comme ça, si vous avez beaucoup d'utilisateurs, vous déployez sur une partie des utilisateurs et si tout se passe bien vous continuez votre déploiement.
Ça peut être très pratique pour un déploiement par vagues.
Ou alors, vous déployez juste pour les nouveaux utilisateurs et au fur et à mesure que les connexions des anciens utilisateurs finissent vous avancez vos déploiement.
Pareil, là, ça vous permet de faire votre déploiement de façon entièrement transparente pour l'utilisateur.
La méthode des VM consistant à rendre l'application indisponible pour l'utilisateur pendant la montée de version peut être révolue.
Avouez que ça déchire quand même.

Ensuite, l'autre bloc, à regarder, c'est le \Definition{c}{replicaset}{objet qui s'assure que le nombre de pods désiré est disponible à un moment donné}.
En gros, le déploiement crée un replicaset en lui disant le nombre de pods que je désire.
Puis, le replicaset crée les pods qui vont bien et s'assure qu'il y en a bien le bon nombre.
Je vous ai montré que si j'en supprimais un, il le recréait.
Mais si j'en crée un manuellement, il va en supprimer un pour la même raison.
Je peux voir que je désire cinq pods, j'en ai actuellement cinq et ils sont tous prêts.

Je ne vais pas reparler des services.
Pour les pods, il n'y a pas non plus besoin de s'attarder, j'en ai déjà parlé.
Je vois qu'il y en a cinq qui tournent, chacun faisant tourner une instance de mon serveur web.

Enfin, je vais expliquer rapidement les parties intéressantes du fichier de configuration.
D'abord, les trois tirets, c'est pour faire comme si c'étaient deux fichiers distincts.
C'est pas souvent une grande idée, surtout si vous avez beaucoup de fichiers complexes.
Mais là, les deux fichiers étant vraiment petits, ça me simplifie la vie.
Ensuite, vous voyez quatre parties qui sont obligatoires~: \liglat{apiVersion}, \liglat{kind}, \liglat{metadata} et \liglat{spec}.
Les trois premiers n'ont pas besoin d'explication, c'est le troisième \liglat{spec} pour «~spécification~» qu'il est important d'expliquer.

Pour le déploiement, il y a d'abord le champ \liglat{replicas} qui dit que je veux cinq instances qui tournent en même temps.
Pour la partie \liglat{containers}, le \liglat{name}, je mets ce que je veux mais il faut que ce soit utilisable comme un nom DNS par Kubernetes.
L'\liglat{image}, c'est le nom de l'image qu'il va trouver sur dockerhub.
Pour le \liglat{metadata}, j'ai besoin d'un \liglat{label} dont je peux choisir le nom et la valeur, ici \liglat{monhello} et \liglat{helloweb}.

En faisant attention, et ça me permet de faire la transition vers le service, à ce que ce soit la même chose définie dans le \liglat{selector} du service.
C'est ce qui permet à Kubernetes de savoir quel déploiement doit être vu du monde entier.
Parce que là, c'est simple, j'ai juste une application web qui affiche une page d'une simplicité inouïe.
Sauf que si vous avez une application web classique avec un serveur applicatif et un serveur de base de données, vous voulez que votre serveur applicatif soit accessible depuis Internet.
Mais vous ne voulez surtout pas que votre serveur de base de données le soit, elle ne doit être accédée que par votre serveur applicatif.
C'est pour ça qu'il faut quand même faire un peu attention à ce qui est exposé.
Ensuite, pour les \liglat{ports}, c'est pareil qu'avant, il y a toujours les ports à mapper comme ils étaient mappés précédemment.

Il y a juste une subtilité.
C'est le \liglat{nodePort} que je n'avais pas précisé initialement et qui me permet de choisir sur quel port accéder au service avec un navigateur.
Ce n'est pas obligatoire mais si je ne précise rien, il va en choisir un au hasard pour moi.
Et à chaque fois que je voudrais y accéder il faudra que j'analyse son choix.
Alors que là, je suis sûr que ce sera toujours le même port qui sera accessible.

Donc, là, vous voyez l'intérêt du ficher par rapport aux commandes.
Vous mettez votre fichier sur un environnement de gestion de versions comme gitlab.
Et vous configurez votre fichier \liglat{.gitlab-ci.yml} avec un \liglat{kubectl apply -f} dessus et à chaque déploiement vous avez vos nouveautés disponibles.
Bon, OK, le fichier \liglat{.gitlab-ci.yml} peut être délicat à écrire avec les déploiements en intégration, les tests automatisés et l'attente d'une validation manuelle pour mise en prod.
Mais dans le principe, vous pouvez automatiser tous vos déploiements.
Je sais, vous pouviez le faire avant Kubernetes mais là, c'est prévu pour.

%end-include

