%begin-include
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Auteur : Stéphane CARPENTIER
% Fichier : interne.tex
% 	Modif : dim. 07 janv. 2024 10:19
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\Section{Et comment ça marche \texorpdfstring{?}{\?}}

Bon, je vous ai montré comment faire pour mettre en place votre application à haute disponibilité en load balancing.
Mais comment ça marche en interne~?
Parce que le but n'était pas de faire un autre tuto mais bien de comprendre les entrailles de Kubernetes.

Là, je ne vais donc pas m'intéresser à ce que je déploie, mais à ce qui gère ce que je déploie.
C'est le \Definition{c}{control plane}{gestionnaire du cluster}.
Comme le but est de gérer des micro-services, ils ont commencé par montrer l'exemple.
Le control plane n'est donc pas un contrôleur monolithique mais un ensemble de composants.
Sur tous les schémas qui existent, le control plane est montré comme en dehors des nodes, la documentation officielle ne fait pas exception.
Heureusement, il est quand même montré à l'intérieur du cluster.
Cependant, comme ce n'est pas de la magie, il faut bien qu'il soit exécuté quelque part.
Or, un node c'est une machine physique ou virtuelle et le cluster c'est l'ensemble des machines physiques et virtuelles disponibles, les schémas sont donc faux.
Parce que les éléments du control plane tournent forcément sur un node.
Vous allez me dire qu'ils savent ce qu'ils font, mais honnêtement, ça prête plus à confusion qu'autre chose.
Si vous êtes riche et que vous avez beaucoup de nodes à votre disposition, les éléments du control plane peuvent tourner sur un ou plusieurs nodes dédiés.

Par contre, si vous êtes comme moi et que vous avez installé minikube sur un ordinateur pour découvrir, vous n'avez qu'un seul node.
Sur lequel tournent à la fois vos applications et les éléments du control plane.
Or, le but de Kubernetes est de s'occuper de vos ressources physiques en vous les masquant.
Vous gérez vos ressources de façon logique et Kubernetes s'occupe du côté physique.
Je ne vais donc pas essayer de faire apparaître la vision logique et la vision physique sur le même schémas, c'est trop compliqué et source de confusion.
Je m'intéresse d'abord au côté physique et je regarde ce qui tourne chez moi.

\begin{listbash}
(stef)> kubectl get pods --namespace=kube-system
NAME                               READY   STATUS    RESTARTS   AGE
coredns-74ff55c5b-l5wh4            1/1     Running   10         12d
etcd-minikube                      1/1     Running   10         12d
kube-apiserver-minikube            1/1     Running   10         12d
kube-controller-manager-minikube   1/1     Running   10         12d
kube-proxy-tlx26                   1/1     Running   10         12d
kube-scheduler-minikube            1/1     Running   10         12d
storage-provisioner                1/1     Running   18         12d
\end{listbash}

Le namespace du kube-system, c'est la gestion centrale, c'est ce qui m'intéresse.
Puisque c'est minikube qui met en place tout ce qui est nécessaire pour Kubernetes, commençons par regarder ça.
Et les quatre pods, ce sont les pods qui sont définis dans toutes les docs de Kubernetes.
Et là, je suis particulièrement ennuyé parce que j'ai beau avoir cherché, je n'ai pas trouvé de preuve dans les configurations et descriptions de tout ce que j'ai trouvé.
Je sais à quoi servent ces pods (ou au moins certains d'entre eux), mais je ne vois pas comment vous le montrer.
Je n'ai trouvé aucune ligne de commande qui fasse le lien entre ces pods et ce que je vais vous expliquer.
Je n'ai pas cherché activement pour tous les pods, j'ai beaucoup cherché pour le plus important et ai abandonné.
Je suis sûr que quand j'écris \liglat{kubectl n'importequoi} c'est le pod \liglat{kube-apiserver-minikube} qui est appelé.
J'ai bien trouvé des pistes avec les endpoints et le port \liglat{8443}, mais j'aurais aimé trouver plus convainquant.
Vous allez donc devoir me faire confiance sur parole.
Je n'aime pas ça et j'aurais aimé trouver mieux.

Ça va avoir l'avantage de raccourcir les explications.
Je vais me contenter d'expliquer sans montrer à quoi ça correspond sur mon ordinateur.
C'est vraiment une des raisons de la complexité du système.
Toutes les docs expliquent les concepts dont je n'ai pas encore parlé et que je vais expliquer maintenant.
Mais vous pouvez très bien déployer vos applications sans savoir ce qu'il se passe derrière.
Et faire le lien entre les concepts et ce que vous faites peut s'avérer difficile.

Donc, à tout seigneur, tout honneur, commençons par le plus central.
Pour repartir sur la gestion des bateaux à grande échelle, il y a forcément un moyen par lequel les informations peuvent transiter.
Que ce soit le PDG qui donne un ordre ou le capitaine d'un bateau qui ait besoin de remonter des informations au PDG.
C'est par là que toutes les décisions doivent passer.
Pour Kubernetes, c'est ce qui est appelé l'API, c'est le pod \liglat{kube-apiserver-minikube}.
C'est ce qui est appelé par la commande \liglat{kubectl}.
C'est par lui que tout passe.
C'est par l'API que les éléments du control plane échangent~: ils ne se contactent pas entre eux directement.

Maintenant nous avons un moyen de communiquer, mais qui communique~?
Si vous faite du commerce maritime international, vous avez forcément une société organisée.
La société possède un comité directeur qui est chargé de prendre toutes les décisions.
Bon, d'accord, dans la vraie vie, le PDG délègue les tâches journalières pour privilégier la stratégie à long terme sur une grosse société.
Mais simplifions et imaginons qu'il veuille garder la main.
C'est lui qui est chargé de savoir s'il y a le bon nombre de bateaux disponibles.
C'est à lui de prendre des décisions quand un bateau coule ou doit être vendu ou acheté.
Pour Kubernetes, c'est le \Definition{c}{controller-manager}{objet qui s'assure que les objets lancés sont disponibles et conformes à ce qui a été demandé}.
Pour minikube, c'est le pod \liglat{kube-controller-manager-minikube}.

Par contre, ce n'est pas lui qui se charge de savoir où déployer le container.
Les containers sont un peu particulier ici puisqu'ils sont jetables.
Si un bateau coule, les containers qu'il transportait sont perdus.
Mais dans notre cas, nous pouvons imaginer que les containers sont recréés à l'envie.
Lorsque le controller manager détecte qu'un pod ou qu'un node est indisponible, c'est lui qui peut décider de relancer un container.
Mais c'est le rôle du \Definition{c}{kube-scheduler}{objet qui regarde les pods nouvellement créés non assignés à un node pour décider sur quels nodes les déployer}.
Le principe est que si un node est devenu injoignable, il ne faut pas déployer le pod dessus.
De même qu'il ne faut pas déployer un pod contenant l'application sur le même node que celui sur lequel le control plane est déployé.
Sauf si c'est la seule possibilité.
Comme par exemple dans mon cas où j'ai minikube qui fait tourner un cluster avec un seul node.

Le principe est de s'assurer que le control plane est toujours disponible.
Vous allez me dire que ce qui vous intéresse, c'est l'application.
Ce qui, en soi, est vrai.
Sauf que, si le control plane est indisponible, l'application l'est aussi.
Sauf, aussi, que si le control plane est indisponible, vous n'avez plus aucun moyen de rétablir l'application.
C'est donc extrêmement critique.

Maintenant que nous avons vu qui prenait les décisions et comment elles étaient communiquées, il reste un détail.
Comment est-ce que les décisions peuvent être prises~?
Si vous gérez beaucoup de bateaux, avec beaucoup de containers par bateau, vous avez besoin d'informations.
Vous devez savoir où sont vos bateaux, où ils doivent aller, qu'est-ce qui est dessus et toussa.
C'est là que la base \Definition{c}{etcd}{stockage à haute disponibilité de toutes les valeurs du cluster}, entre en jeu.
À chaque fois qu'un modification est faite ou demandée, elle y est stockée.
À chaque fois qu'un changement est détecté, c'est mis à jour.
En gros, c'est la mémoire centrale de toutes les informations importantes.

Voilà, nous avons défini tout ce qui est obligatoire pour composer le control plane.
Mais, le control plane contrôle ce qui est stocké sur des nodes.
Il faut donc qu'il puisse communiquer avec ces nodes.
C'est là que \Definition{c}{kube-proxy}{proxy réseau qui implémente le concept de service sur chaque node}, intervient.
À vue de nez, je dirais que sur mon node, qui est un cluster à lui tout seul, c'est le pod \liglat{kube-proxy-tlx26} qui fait ce travail.
Mais le node, une fois que le scheduler lui a dit qu'il fallait qu'il lance le pod, comment il s'y prend~?

C'est le travail du dernier composant obligatoire qui est lui aussi déployé sur chaque node.
C'est \Definition{c}{kubelet}{agent qui s'assure que chaque container s'exécute bien dans un pod}.
Pour le coup, je ne vais pas le dire trop fort.
Sur ma machine, il ne reste que deux pods qui n'ont pas été définis.
Et comme j'ai bien une application web qui tourne avec de la répartition de charge, c'est que tout est bon.
Donc, c'est l'un des deux.
Or, le pod \liglat{coredns-74ff55c5b-l5wh4} n'a pas un nom à faire ça.
Il a plutôt un nom à être associé à un service DNS pour que je puisse utiliser des noms et pas des IP.
Reste le pod \liglat{storage-provisioner} qui a un nom qui pourrait convenir en fonction de ce qu'on entend par là.
Je vais donc dire que c'est celui là, à défaut de mieux, mais sans conviction.
Je passe rapidement à la suite.

Là, je vous ai expliqué la vision physique de Kubernetes.
C'est à dire que c'est la façon dont Kubernetes gère ses ressources.
Normalement, pour vous, ce n'est pas votre problème.
Vous avez vu, sur mon déploiement j'ai dit ce que je voulais.
Je n'ai pas dit où je voulais que ce soit déployé.
Comme je n'ai qu'un nœud, il était obligé de tout déployer au même endroit.
Vous pouvez l'aider à gérer ses ressources, mais rien n'est obligatoire.

Vous, ce qui vous intéresse, c'est la vision logique.
Vous voulez que vos containers soient joignables ou pas, se parlent ou pas.
Cela n'a pas de lien avec les machines sur lesquelles ils tournent.
Lorsque vous avez bien décrit l'organisation de vos pods, par exemple, vous avez dit que vous voulez faire tourner une application web en frontal avec une base de données en back-end.
Vous voulez que votre service applicatif soit joignable d'Internet.
Vous ne voulez pas que votre base de données le soit.
Vous voulez faire de la répartition de charge et vous avez quatre machines qui tournent pour gérer tout ça.
Vous aurez donc une machine dédiée au control plane et trois machines pour faire tourner vos serveurs de façon redondante.
Comme vous vous êtes bien énervé, vous avez tout peaufiné et ça tourne du feu de dieu de façon sécurisée comme il faut.

Et c'est là que c'est beau.
S'il y a une machine qui crashe, vous n'avez rien à faire, Kubernetes va remarquer qu'il y a un node injoignable et ne va plus l'utiliser.
Comme vous n'avez pas dit où devaient tourner vos pods, vous n'avez rien à faire au niveau déploiement.
Bien sûr, si vous aviez besoin des machines, votre application va devenir un peu poussive, mais par rapport à une indisponibilité totale, c'est très acceptable.
Mais ce n'est acceptable que sur du court terme.
Votre application continue à être disponible pendant que vous réparez votre machine.
Ou que vous mettez une machine neuve à la place.
Une fois que votre nouveau node est configuré pour être intégré à votre cluster, c'est fini pour vous.
Ensuite, c'est Kubernetes qui va utiliser le nouveau node sans que ça n'ait le moindre impact sur la définition logique que vous avez faite.

%end-include

