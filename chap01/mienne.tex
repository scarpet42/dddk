%begin-include
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Auteur : Stéphane CARPENTIER
% Fichier : mienne.tex
% 	Modif : mer. 03 janv. 2024 22:14
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\Section{Mon image de Docker}

Maintenant, les choses sérieuses commencent.
Je vais créer une image qui me permette de faire un \liglat{ping}.
Je vais faire le minimum vital, façon «~j'ai lu que le début de la doc pour voir les impacts~».
Ça va illustrer le fait que pour utiliser Docker et Kubernetes, il ne suffit pas de lire un tuto sur Internet~: il faut vraiment comprendre ce qu'il se passe en interne.
Sinon, vu de loin, ça peut sembler marcher mais les conséquences à long terme peuvent être importantes en environnement de prod.

Pour créer mon image, c'est très simple, j'ai juste besoin d'un fichier Dockerfile.
Le nom de ce fichier n'est pas obligatoire.
Cependant, comme il permet à tout le monde de savoir ce qu'il contient et qu'il évite de saisir son nom sur la ligne de commande, je vais l'utiliser~: c'est propre.

\begin{monfichier}{dockerfile}{Dockerfile}
#Première image de ping et curl pour démo
FROM ubuntu
RUN apt-get update
RUN apt-get install inetutils-ping -y
RUN apt-get install curl -y
\end{monfichier}

C'est pas très compliqué à comprendre.
La première ligne est un commentaire.
La seconde ligne est pour dire que je vais partir d'une image ubuntu.
Les trois autres lignes sont pour exécuter les commandes lancées dans le terminal.
La première étant nécessaire pour mettre le système à jour avant d'installer les packets.
Pour le \liglat{-y} à la fin des commandes, c'est parce que je veux qu'il le fasse sans me demander confirmation.
Il n'y a plus qu'à demander à Docker de le lancer.
Là encore, c'est simple, le \liglat{build}, c'est pour dire à Docker de construire l'image.
L'option \liglat{-t} c'est pour tag, pour donner le nom de l'image qui s'appellera \liglat{monclient}.
Le \liglat{.} final, c'est du shell de base, c'est le nom du répertoire dans lequel je me trouve quand je lance ma commande.
C'est pour dire que mon fichier Dockerfile est dans le même répertoire que moi.
C'est parti~:

\begin{listbash}
(dddk)> docker build -t monclient .
Sending build context to Docker daemon  2.048kB
Step 1/4 : FROM ubuntu
...[SNIP]...
Successfully tagged monclient:latest
\end{listbash}

Là encore, il y a plein de lignes, je zappe en gardant juste le début et la fin pour montrer que tout s'est bien déroulé.
Je montre aussi qu'il y a quatre étapes qui correspondent aux quatre lignes du fichier.

Allons voir ce que ça a donné~:

\begin{listbash}
(stef)> docker images
REPOSITORY       TAG       IMAGE ID       CREATED             SIZE
monclient        latest    0cc2f90160c8   3 minutes ago       116MB
ubuntu           latest    f643c72bc252   4 weeks ago         72.9MB
\end{listbash}

C'est fait, j'ai une image, par contre, cette image est très grosse.
En ajoutant juste deux petites commandes (un peu plus de 250 ko pour les deux), j'ai augmenté la taille de l'image de plus de 50\% (quelques dizaines de Mo).
Vu qu'il n'y avait pas beaucoup de lignes, il n'a pas pu y avoir beaucoup d'erreurs.
Imaginez sur un gros fichier avec beaucoup de lignes ce que ça peut donner.
Mais au moins, est-ce que ça fonctionne comme je le voulais~?

\begin{listbash}
(stef)> docker run monclient ping exemple.com
PING exemple.com (107.180.40.145): 56 data bytes
64 bytes from 107.180.40.145: icmp_seq=0 ttl=43 time=81.705 ms
64 bytes from 107.180.40.145: icmp_seq=1 ttl=43 time=81.320 ms
64 bytes from 107.180.40.145: icmp_seq=2 ttl=43 time=81.173 ms
^C--- exemple.com ping statistics ---
3 packets transmitted, 3 packets received, 0% packet loss
round-trip min/avg/max/stddev = 81.173/81.399/81.705/0.224 ms
\end{listbash}

Oui, c'est bon.
J'ai maintenant une image qui me permet de faire des \liglat{ping} et des \liglat{curl}.
L'intérêt est double.
D'abord, il me fallait une image personnalisée simple pour m'assurer que ça marche.
La seconde est que j'ai besoin de comprendre Docker pour le boulot.
Or, au boulot, il y a des serveurs dans des Dockers qui vont interagir.
Je ne vais pas installer une application avec un serveur applicatif complet et sa base de données juste pour découvrir.
Maintenant, j'ai donc un \liglat{ping} et un \liglat{curl} installés dans une image qui me permet d'interagir avec un autre serveur lancé à partir de son image.

Sauf que, comme je l'ai dit, je veux un truc simple.
Et là, c'est beau, ils ont tout prévu, il existe nginx qui propose un serveur léger et sécurisé et tout ça.
Voyons voir ça.

\begin{listbash}
(stef)> docker run nginx
Unable to find image 'nginx:latest' locally
latest: Pulling from library/nginx
...[SNIP]...
/docker-entrypoint.sh: Configuration complete; ready for start up
\end{listbash}

Et voilà, ça tourne.
Comment ça marche~?

\begin{listbash}
(stef)> docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED              STATUS              PORTS     NAMES
323405febcc4   nginx     "/docker-entrypoint.…"   About a minute ago   Up About a minute   80/tcp    modest_feistel
\end{listbash}

Il écoute sur le port \liglat{80}, donc, c'est parti.

\begin{listbash}
(stef)> curl localhost
curl: (7) Failed to connect to localhost port 80: Connexion refusée
\end{listbash}

Et depuis mon image à moi que j'ai faite rien que pour ça~?

\begin{listbash}
(stef)> docker run monclient curl localhost
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
curl: (7) Failed to connect to localhost port 80: Connection refused
\end{listbash}

Pareil.
C'est bien beau d'avoir un serveur qui tourne si je ne peux pas y accéder.
Puisque le nom donné par le \liglat{ps} était \liglat{modest_feisel}, je vais demander à Docker s'il n'aurait pas des informations intéressantes dessus.

\begin{listbash}
(stef)> docker inspect modest_feistel | grep -i ipadd
            "SecondaryIPAddresses": null,
            "IPAddress": "172.17.0.2",
                    "IPAddress": "172.17.0.2",
\end{listbash}

Donc, j'ai une IP privée.
Voyons-voir ça.

\begin{listbash}
(stef)> curl 172.17.0.2
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
\end{listbash}

Et ça marche.
Vous pouvez voir que ce n'est pas la peine d'avoir un navigateur moderne pour afficher ça.
Comme je l'ai dit, c'est minimal.
Le but est d'installer son propre site Internet.
À partir de là, je vais faire le même test depuis mon image pour voir ce que ça donne.

\begin{listbash}
(stef)> docker run monclient curl 172.17.0.2
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   612  100   612    0     0   597k      0 --:--:-- --:--:-- --:--:--  597k
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
\end{listbash}

C'est pareil.
Ça veut donc dire que, lorsque j'utilise un serveur avec Docker, par défaut il n'est visible que de son adresse IP privée.
C'est sécurisé puisque personne ne peut y accéder directement hors de ma machine.
Il est aussi visible depuis une autre installation avec Docker.

C'est bien beau tout ça, mais je ne veux pas connaître son adresse IP privée pour y accéder.
Son adresse IP privée, je ne l'ai pas donnée, c'est Docker qui l'a choisie pour moi.
Elle peut donc changer à l'envie et je veux quelque chose de stable.

C'est facile, il suffit de demander à Docker de mapper les ports.
C'est l'option \liglat{-p 80:80}, le premier \liglat{80}, c'est le port sur lequel écoute nginx et le second \liglat{80}, c'est le port sur lequel il doit être visible.
Voyons voir ce que ça donne.

\begin{listbash}
(stef)> docker run -p 80:80 nginx
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
\end{listbash}

Et donc, maintenant pour afficher le site~:

\begin{listbash}
(stef)> curl localhost
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
\end{listbash}

C'est beau, ça marche.
Si ça semble un peu lourd, c'est vraiment pratique pour plein de raisons~:

\begin{tuxliste}
\item Par défaut rien n'est visible de l'extérieur. C'est une sécurité non négligeable.
\item Il est possible d'en faire tourner plusieurs simultanément chacun sur son port. Certains étant visibles du reste du monde et d'autres pas.
\item Il est possible de faire des déploiements de façon particulièrement simples. Lorsque l'image est construite, c'est son lancement qui défini si c'est de la production ou du test. Un retour arrière se fait donc sans modification de l'image.
\end{tuxliste}


%end-include

