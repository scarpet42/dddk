'use strict';

// Pour pouvoir récupérer le hostname
const os = require('os');
const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';
const HOSTNAME = os.hostname();

// App
const app = express();
app.get('/', (req, res) => {
  res.send(`Bonjour le monde depuis ${HOSTNAME}`);
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
