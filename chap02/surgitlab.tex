%begin-include
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Auteur : Stéphane CARPENTIER
% Fichier : surgitlab.tex
% 	Modif : dim. 05 mars 2023 21:18
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\Section{C'est parti pour utiliser mon image}

J'ai deux possibilités.
La première, c'est de rebuilder l'image avec le bon tag.
Mais c'est long, ça prend plusieurs minutes.
Je vais donc utiliser la seconde possibilité~: lui associer le tag qui va bien et je supprimerai l'autre.
Remarquez que je ne suis pas obligé de donner la totalité du hash quand il n'y a pas de confusion possible.

\begin{listbash}
(stef)> docker images
REPOSITORY       TAG       IMAGE ID       CREATED         SIZE
archlatex        latest    7cd892687b36   2 hours ago     3.11GB
(stef)> docker tag 7cd89 scarpet42/latex:latest
(stef)> docker images
REPOSITORY        TAG       IMAGE ID       CREATED         SIZE
archlatex         latest    7cd892687b36   2 hours ago     3.11GB
scarpet42/latex   latest    7cd892687b36   2 hours ago     3.11GB
(stef)> docker rmi archlatex:latest
Untagged: archlatex:latest
(stef)> docker images
REPOSITORY        TAG       IMAGE ID       CREATED         SIZE
scarpet42/latex   latest    7cd892687b36   2 hours ago     3.11GB
\end{listbash}

Et voilà, c'est bon.
J'ai plus qu'à envoyer mon image sur dockerhub.
D'abord je me connecte et je pousse.

\begin{listbash}
(stef)> docker login --username=scarpet42
Password:
WARNING! Your password will be stored unencrypted in /home/stef/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
(stef)> docker push scarpet42/latex
Using default tag: latest
The push refers to repository [docker.io/scarpet42/latex]
ffaf1fa05a79: Pushed
4ae2f8cabcc7: Pushed
49816f55d045: Pushed
3ed0cee20f76: Mounted from library/archlinux
f99542f4040b: Mounted from library/archlinux
latest: digest: sha256:7e552f554309e68fd73b7984bad5316371966b14fedb131185a6073bbf2be27c size: 1367
\end{listbash}

Et voilà, mon image est sur dockerhub, vous pouvez la récupérer si vous voulez.
Si vous n'en voulez pas, c'est pas grave, c'est pour moi que je l'ai faite.
Je n'ai plus qu'à modifier mon fichier \liglat{.gitlab-ci.yml} pour le prendre en compte.

\begin{monfichier}{yaml}{.gitlab-ci.yml}
stages:
  - build
  - deploy

# L'utilisation de la variable avec le nom du projet me permet d'utiliser ce fichier
# dans tous mes projets

CompilePdf:
  stage: build
  image: scarpet42/latex # C'est le mien à moi que j'ai fait
  script:
    - latexmk -pdf -shell-escape ${CI_PROJECT_NAME}.tex
  artifacts:
    paths:
      - ${CI_PROJECT_NAME}.pdf

pages:
  stage: deploy
  script:
    - mkdir public
    - cp index.html public
    - cp ${CI_PROJECT_NAME}.pdf public
  artifacts:
    paths:
      - ${CI_PROJECT_NAME}.pdf
      - public
\end{monfichier}

Ce fichier est très court, très simple, mais il y a beaucoup de choses intéressantes dedans.
Ce n'est pas un cours sur le CI/CD de gitlab, mais quelques explications s'imposent pour comprendre à la fois le principe et l'intérêt.

Il y a deux choses à comprendre de façon générale~: les jobs et les stages.
Ensuite, tout le reste est logique.
Dans le cas présent, il y a deux jobs et deux stages.

D'abord, un \Definition{c}{stage}{ou une étape de traitement du fichier}, comme le nom le laisse supposer.
Ici, comme je l'écrivais, je n'ai que deux étapes.
Une pour compiler mes sources de façon à avoir un beau fichier pdf, elle s'appelle build.
Mais un beau pdf, si c'est pour être prisonnier d'un container, ça ne sert à rien.
Donc, ma deuxième étape consiste à mettre mon superbe document à la disposition du monde entier.
Surtout à la mienne en fait.
C'est le déploiement, appelé ici \liglat{deploy}, comme son nom l'indique.
La seconde étape n'est pas exécutée si l'étape précédente s'est plantée.

Ces deux étapes sont minimales, dans un vrai projet, il y a plus d'étapes~: il y a des déploiements dans divers environnements, des tests, et plein d'autres choses.
Mais là, mes tests, je les ai déjà fait chez moi, à part les spécificités liées à gitlab, je sais que ça fonctionne.
Et les spécificités liées à gitlab, je sais quand elles peuvent arriver et je les corrige tout de suite.
Par exemple, lorsque je déploie un projet pour la première fois sur gitlab, je peux avoir oublié quelque chose.
Ou alors, lorsque je modifie mon fichier \liglat{.gitlab-ci.yml} je peux m'y être mal pris.
Une typo est si vite arrivée ou, comme dans le cas présent, lorsque je change d'image.
Ce n'est pas parce que je trouve mon image en local que gitlab va la trouver, même si ça a marché du premier coup et pour le coup je suis trop content.

Une fois les étapes définies, il s'agit de définir ce qui doit être fait à chaque étape.
Et c'est là que les jobs entrent en jeu.
Le \Definition{c}{job}{ou travail en anglais, consiste à lister les commandes qui doivent être exécutées}, comme son nom l'indique là aussi.
Ça se traduit par un ensemble de commandes et de conditions.
Ici, je n'ai qu'une condition par job, à savoir qu'une étape n'exécute qu'un job.
Mais une étape pourrait très bien lancer plusieurs jobs.
De même qu'un job pourrait être lancé dans plusieurs étapes.
Par exemple, je pourrais très bien lancer les mêmes tests à chaque étape.
Ou je pourrais avoir une étape de déploiement en intégration avec construction, test et déploiement puis une autre étape de déploiement en prod avec juste le déploiement.
C'est vraiment une question d'organisation, il y a une très grande liberté ici.

Pour les conditions, je pourrais avoir d'autres conditions, comme ne déployer que la branche principale.
Ou alors déployer manuellement en prod et déployer automatiquement en intégration.
Quand je parle de déploiement manuel, je ne parle pas de lancer manuellement toutes les commandes qui auraient été exécutées automatiquement.
Je parle bien de lancer le script manuellement, c'est à dire que la décision de pousser en prod est humaine.
Mais ensuite, tout ce qui a été automatisé est utilisé.

Il y a d'autres choses qui ne sont ni vraiment des scripts ni vraiment des conditions.
Ici, j'utilise des artifacts.
Ce sont des fichiers qui sont attachés au job après qu'il ait fini.
Ici, le seul fichier qui m'intéresse est le fichier pdf, je n'ai absolument pas besoin des fichiers de logs et de construction une fois la compilation finie.
Si je n'utilisais pas l'artifact, je serais obligé de recompiler mon fichier pdf pour le déploiement.

Vous voyez aussi qu'il y a des variables entre parenthèses avec un dollar devant.
J'ai la possibilité de définir mes propres variables, mais je n'en ai pas eu besoin.
J'ai utilisé une variable mise à disposition par gitlab, qui est le nom du projet.
J'utilise ce nom à la fois comme nom de répertoire en local, comme nom de fichier source et compilé.
Comme j'ai plusieurs projets, je peux utiliser le même fichier dans tous mes projets sans avoir à changer le nom, c'est pratique.

Maintenant que le contexte est expliqué, c'est facile de comprendre le fichier.
Ma première étape, comme son nom l'indique, compile le pdf.
Je récupère mon image sur dockerhub, je compile et je sauvegarde le pdf.

Puis, la seconde étape, comme son nom ne l'indique pas, met le pdf à disposition du monde entier.
Le nom du stage est très important, c'est avec ce nom que gitlab sait qu'il faut déployer sur le site Internet, mis à disposition par gitlab, qui est lié à mon projet.
Dans cette étape, je crée un répertoire \liglat{public} dans lequel j'y mets l'index et mon fichier pdf compilé à l'étape précédente.
Le nom du répertoire est, lui aussi, très important~: c'est ce qui est dedans qui sera visible pour le monde entier.
Le fichier d'index était écrit en local et déployé avec mon code.

Voilà, vous avez toutes les cartes en main pour bien comprendre le fichier.


%end-include

