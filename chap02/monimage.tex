%begin-include
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Auteur : Stéphane CARPENTIER
% Fichier : monimage.tex
% 	Modif : mer. 03 janv. 2024 22:44
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\Section{On n'est jamais mieux servi que par moi-même}

Donc, voilà, c'est à mon tour de me faire ma propre image.
Je ne vais pas juste faire un copier-coller du Dockerfile de l'image qu'il utilisait pour deux raisons.
\begin{tuxliste}
\item Je veux essayer d'avoir une image plus petite que la sienne.
\item J'utilise une extension qui n'est pas dans les distributions \LaTeX{} et je vais la mettre dans l'image plutôt que de la copier dans tous mes documents.
\end{tuxliste}

Le travail va consister à partir d'une image existante pour installer tout ce que je veux dessus et voir ensuite comment l'optimiser.
Pour comparer, j'ai récupéré quelques images, mais la taille initiale ne fait pas tout.
Seuls les packages inutiles installés sur la distribution de base augmentent inutilement la taille finale.

\begin{listbash}
(stef)> docker images
REPOSITORY       TAG       IMAGE ID       CREATED         SIZE
archlinux        latest    c689d2874bf2   12 days ago     407MB
alpine           latest    389fef711851   2 weeks ago     5.58MB
debian           testing   620be443bfad   3 weeks ago     120MB
debian           latest    6d6b00c22231   3 weeks ago     114MB
ubuntu           latest    f643c72bc252   5 weeks ago     72.9MB
phil9909/latex   latest    d1bd4e48f0f0   23 months ago   4.63GB
\end{listbash}

Déjà, je vois que quelque soit la taille de l'image initiale, elle fait largement moins de 4Go de moins que la taille de l'image que je veux optimiser.
Ça veut dire que si j'installe bien mes packages, et que j'en installe moins que 4Go j'aurai réussi.
J'ai aussi récupéré une distribution qui doit vous intriguer.
Lorsque vous regardez la distribution alpine, vous voyez qu'elle est vraiment minimale.
Ça pourrait sembler être la meilleure idée de l'utiliser puisque je serais sûr de n'installer que ce qui est nécessaire.
Le problème, c'est que si cette distribution est minimale, sa doc l'est aussi et son gestionnaire de package semble aussi l'être.
Je peux faire des essais et découvertes, mais sans doc alors que je suis en train de découvrir Docker et Kubernetes et qu'en plus, l'image \LaTeX{} disponible pour alpine ne fonctionne pas, tout ça cumulé ne me motive pas.
Je garde donc cette distribution en visibilité.
Si plus tard j'essaye de diminuer la taille de mon image et qu'ils ont une doc qui tient la route, c'est possible que je m'y intéresse\NDAuteur{En fait, je me suis renseigné sur la distribution alpine depuis. C'est une distribution très légère qui a remplacé la glibc par musl et qui a enlevé toutes les options rarement utilisées des programmes de base pour les rendre plus légers. En gros, c'est bien dans la majorité des cas si python n'est pas utile. Comme j'ai besoin de python j'oublie pour l'instant}.

En attendant, je vais commencer par voir ce que je peux faire à partir d'Archlinux.
Je pars d'un truc basique, le but est de le faire fonctionner puis de l'améliorer.
Donc, un fichier qui doit juste installer les packages minimums.
Si vous regardez les tailles des images au dessus, vous verrez que déjà pour une installation de test, il est préférable d'avoir une bonne connexion réseau.
Normalement, je prends tout \LaTeX{} sauf les parties dédiées aux langues non latines (tout sauf Grec, Cyrilique, Chinois, Japonais et Coréen).
Je ne dois pas en avoir besoin et ça m'économise plus d'un Go.

\begin{monfichier}{dockerfile}{Dockerfile}
FROM archlinux
RUN pacman -Syu --noconfirm texlive-most texlive-langextra python-pygments
\end{monfichier}

Ce qui donne~:

\begin{listbash}
(docker)> docker build -t monlatex .
Sending build context to Docker daemon  20.48kB
Step 1/2 : FROM archlinux
...[SNIP]...
Total Download Size:    857.70 MiB
Total Installed Size:  2441.46 MiB
Net Upgrade Size:      2378.23 MiB
...[SNIP]...
Successfully built e777b4ec4254
Successfully tagged monlatex:latest
\end{listbash}

Je n'ai récupéré ni pandoc ni fig2dev parce que ça prend de la place et que je n'en ai pas besoin.
Pour moi, ce sont des packages un peu différents.
Voyons donc ce que ça donne.

\begin{listbash}
(stef)> docker images
REPOSITORY       TAG       IMAGE ID       CREATED         SIZE
monlatex         latest    e777b4ec4254   4 minutes ago   3.96GB
archlinux        latest    c689d2874bf2   12 days ago     407MB
alpine           latest    389fef711851   2 weeks ago     5.58MB
debian           latest    6d6b00c22231   3 weeks ago     114MB
phil9909/latex   latest    d1bd4e48f0f0   23 months ago   4.63GB
\end{listbash}

C'est vraiment énorme.
Je pars de 400Mo, j'installe 2,4GO et je me retrouve avec une image de 4Go.
Il y a plus d'1Go en trop.
Le problème d'archlinux, c'est qu'ils n'ont pas compris l'intérêt de Docker.
Je suis allé chercher pourquoi j'avais ça et c'est la conclusion à laquelle d'autres personnes sont aussi tombées.
Sur un ordinateur personnel, ils installent une distribution minimale.
Mais sur une image Docker, ils veulent que ce soit minimal, mais ils veulent aussi que ce soit totalement fonctionnel.
C'est à dire qu'il y a plein de docs, pleins de programmes qui pourraient me servir au cas où.
Bref, tout l'inverse de la philosophie de Docker.

Je vous passe les détails, j'ai regardé ce qui prend de la place pour voir où je pouvais faire du ménage.
Sur les 2,5Go de \LaTeX{}, les fontes prennent presque 2Go.
C'est clair, je n'ai pas besoin de toutes les fontes, mais pour l'instant, je vais laisser comme ça.
Ensuite, il y a presque 1Go de cache pour pacman (le gestionnaire de packages d'archlinux).
Le seul intérêt de ce cache, c'est de pouvoir simplifier les futures mises à jour.
Mais mon but n'est pas de mettre à jour l'image finale, il est de l'utiliser.
Je vais donc vider tout ça, et comme il pose des difficultés et que c'est moi qui commande non mais, je vais le faire avec un \liglat{rm} en bypassant le gestionnaire.
Sur une installation normale, ce n'est pas forcément une grande idée puisque ça peut rendre son gestionnaire de packages instables.
Sur mon image je n'en aurai pas besoin, j'y vais.

Et voici mon nouveau Dockerfile.
Pour les observateurs qui remarquent que j'installe \liglat{which}, oui effectivement, il faut l'installer manuellement parce que ce n'est pas sur le Docker officiel d'Archlinux.
C'est un packet qui fait 34ko une fois installé et il n'est pas installé alors que c'est une commande standard et que l'image fait 500Mo.
C'est assez surprenant et j'ai eu du mal à le trouver.
Je ne vais pas montrer les essais/erreurs pour le trouver parce que là, je ne cherche pas à montrer comment débugguer \LaTeX{}, même si c'est dans une image Dockerfile.
Pour ceux qui sont intéressés par l'exercice, vous supprimez le \liglat{which} de mon fichier Dockerfile pour créer l'image.
Une fois l'image créée, ça se passe en deux temps en lançant la compilation.
La première compilation vous montre que \LaTeX{} ne trouve pas un fichier qui est installé.
En vous connectant au container, vous remarquez vite que les fichiers sont installés.
Dans le Dockerfile, vous faites un lien entre le fichier recherché et le répertoire dans lequel \LaTeX{} cherche ses exécutables et vous recréez l'image.
Lors de la seconde compilation, le message d'erreur est plus explicite.
Vous mettez le \liglat{which} pour l'installation, enlevez la ligne créant le lien et vous arrivez ici.

\begin{monfichier}{dockerfile}{Dockerfile}
FROM archlinux
RUN pacman -Syu --noconfirm texlive-most texlive-langextra python-pygments which && pacman -Scc --noconfirm && rm -Rf /var/cache/pacman
\end{monfichier}

Je ne supprime que le cache de pacman parce que j'ai besoin de m'assurer que \LaTeX{} sera fonctionnel.
Maintenant, je lance ce build et je regarde ce que ça donne.

\begin{listbash}
(docker)>  docker build -t archlatex .
(stef)> docker images
REPOSITORY       TAG       IMAGE ID       CREATED         SIZE
archlatex        latest    8eebcc5d28b1   2 minutes ago    3.11GB
phil9909/latex   latest    d1bd4e48f0f0   24 months ago   4.63GB
\end{listbash}

C'est bien ça, j'ai gagné 1,5Go par rapport à l'image de phil9909.
Reste à voir si j'ai tout ce qu'il me faut.

\begin{listbash}
(dddk)> docker run --rm --mount type=bind,source=/home/stef/latex/dddk,target=/tmp --workdir=/tmp archlatex latexmk -pdf  --shell-escape dddk.tex
...[SNIP]...
Output written on dddk.pdf (43 pages, 546275 bytes).
Transcript written on dddk.log.
Latexmk: Index file 'dddk.idx' was written
Latexmk: Log file says output to 'dddk.pdf'
Latexmk: Examining 'dddk.log'
=== TeX engine is 'pdfTeX'
Latexmk: All targets (dddk.pdf) are up-to-date
\end{listbash}

Et c'est bon.
Ce n'est que le début, mais ça commence bien.
Bon, là, le truc simple et propre, c'est d'ajouter un utilisateur pour éviter de lancer des programmes en tant qu'admin qui modifie les fichiers partagés avec le système.
Le plus simple, c'est d'utiliser adduser, sauf que là, c'est pas installé sur le container, je vais donc utiliser useradd pour créer un utilisateur qui a les mêmes id que moi.

\begin{monfichier}{dockerfile}{Dockerfile}
FROM archlinux
RUN pacman -Syu --noconfirm texlive-most texlive-langextra python-pygments which && pacman -Scc --noconfirm && rm -Rf /var/cache/pacman
RUN groupadd -g 1000 -o latex
RUN useradd -d /tmp -u 1000 -g 1000 latex
\end{monfichier}

J'ai créé un groupe et un utilisateur sur deux lignes distinctes.
C'est pas l'idéal de l'optimisation, mais c'est plus lisible.
Je suppose que pacman a probablement modifié les mêmes fichiers avant, la perte de lisibilité pour gagner quelques octets sur une image de 3Go n'est pas justifiée.
Je construis et j'essaye dans la foulée.

\begin{listbash}
(docker)> docker build -t archlatex .
...[SNIP]...
Successfully tagged archlatex:latest
(dddk)> docker run --rm --mount type=bind,source=/home/stef/latex/dddk,target=/tmp --workdir=/tmp --user 1000 archlatex latexmk -pdf -shell-escape dddk.tex
...[SNIP]...
Latexmk: All targets (dddk.pdf) are up-to-date
(dddk)> ls -ltr | tail
drwxr-xr-x  2 stef stef  20480 25 janv. 20:43 _minted-dddk
-rw-r--r--  1 stef stef    195 25 janv. 20:43 dddk.listing
-rw-r--r--  1 stef stef  12110 25 janv. 20:43 dddk.aux
-rw-r--r--  1 stef stef      0 25 janv. 20:43 dddk.mw
-rw-r--r--  1 stef stef   1562 25 janv. 20:43 dddk.toc
-rw-r--r--  1 stef stef    275 25 janv. 20:43 dddk.idx
-rw-r--r--  1 stef stef 186928 25 janv. 20:43 dddk.fls
-rw-r--r--  1 stef stef 549007 25 janv. 20:43 dddk.pdf
-rw-r--r--  1 stef stef  79533 25 janv. 20:43 dddk.log
-rw-r--r--  1 stef stef  46154 25 janv. 20:43 dddk.fdb_latexmk
\end{listbash}

Et voilà, ça roule.
J'ai mon image à moi qui marche et qui fait 1,5Go de moins que celle que j'utilisais.
Maintenant, le but n'est pas de l'utiliser en local.
Le but est de l'utiliser sur gitlab.
J'ai donc besoin de commencer par la pousser sur dockerhub pour que gitlab puisse venir le récupérer sur dockerhub.
Mais en plus, je ne veux pas appeler mon image archlatex, je veux juste l'appeler latex.
Le archlatex, c'est bien pour le cas où j'avais plusieurs images à gérer en parallèle.
Mais là, c'est bon, j'ai mon image et je verrai plus tard pour l'améliorer.
Ensuite, comme pour l'image de phill9909, il faut que je mette mon login pour que ça marche.
Je n'ai pas été très original, j'ai gardé le même login que pour gitlab.


%end-include

