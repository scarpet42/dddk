{ pkgs ? import <nixpkgs> { }
, pkgsLinux ? import <nixpkgs> { system = "x86_64-linux"; }
}:

pkgs.dockerTools.buildImage {
  name = "nix4dockerping";
  tag = "latest";
  created = "now";
  config = {
    Entrypoint = [ "${pkgsLinux.iputils}/bin/ping" ];
    Cmd = [ "example.com" ];
  };
}
