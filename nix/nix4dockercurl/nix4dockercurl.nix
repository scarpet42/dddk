{ pkgs ? import <nixpkgs> { }
, pkgsLinux ? import <nixpkgs> { system = "x86_64-linux"; }
}:

pkgs.dockerTools.buildImage {
  name = "nix4dockercurl";
  tag = "latest";
  created = "now";
  config = {
    Entrypoint = [ "${pkgsLinux.curl}/bin/curl" ];
    Cmd = [ "example.com" ];
  };
}
